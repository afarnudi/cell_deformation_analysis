import pytest

from src.deformation_analysis.tools.general import check_time_frames

def test_time_frame_n1_p2():
     with pytest.raises(ValueError) as exc_info:
        check_time_frames([-1,2])
        assert "Time frame cannot be a negative number, input time frame: -1." in str(exc_info)

def test_time_frame_n1_n3():
     with pytest.raises(ValueError) as exc_info:
        check_time_frames([-1,-3])
        assert "Time frame cannot be a negative number, input time frame: -1." in str(exc_info)

def test_time_frame_4_3():
     with pytest.raises(ValueError) as exc_info:
        check_time_frames([4,3])
        assert "Time frame inconsistency start frame, 4 is larger than the end frame, 3." in str(exc_info)
