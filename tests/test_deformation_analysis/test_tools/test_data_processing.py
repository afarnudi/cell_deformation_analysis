import pytest
import numpy as np

from src.deformation_analysis.tools.data_processing import (
    get_cells_label_list,
    convert_labels_list_to_time_frames,
    check_frame_limits_consistency,
    convert_raw_labels_to_labels,
    get_image_label,
    strip_labels,
    load_pickle_file,
    generate_prefix_suffix_combination,
)


def test_get_cells_label_list():
    dummy_label_to_list_dictt = {
        "name_1": [10001, 20001, 30001, 40001],
        "name_2": [10002, 20002, 30002, 40002, 50002],
        "name_3": [10003, 20003, 30003],
    }
    expected_answer = [
        [10001, 20001, 30001, 40001],
        [10002, 20002, 30002, 40002, 50002],
        [10003, 20003, 30003],
    ]
    assert expected_answer == get_cells_label_list(
        ["name_1", "name_2", "name_3"], dummy_label_to_list_dictt
    )


def test_convert_labels_list_to_time_frames():
    dummy_labels_list = [
        [10001, 20001, 30001, 40001],
        [10002, 20002, 30002, 40002, 50002],
        [10003, 20003, 30003],
    ]
    expected_answer = [
        [1, 2, 3, 4],
        [1, 2, 3, 4, 5],
        [1, 2, 3],
    ]
    assert expected_answer == convert_labels_list_to_time_frames(dummy_labels_list)


def test_check_frame_limits_consistency_inconsistent_cell_frames():
    dummy_frames = [
        [1, 2, 3, 4],
        [5, 6],
        [1, 2, 3],
    ]
    with pytest.raises(RuntimeError) as exc_info:
        check_frame_limits_consistency([1, 3], dummy_frames)
        assert (
            "Could not find an image frame where all the selected cells appear at the same time."
            in str(exc_info)
        )


def test_check_frame_limits_consistency_bad_frame_input():
    dummy_frames = [
        [3, 4],
        [4, 5, 6],
        [3, 4, 5],
    ]
    with pytest.raises(RuntimeError) as exc_info:
        check_frame_limits_consistency([1, 2], dummy_frames)
        assert (
            "The first image frame where all selected cells appear at the same time is"
            in str(exc_info)
        )


def test_check_frame_limits_consistency_edit_input_frame():
    dummy_frames = [
        [2, 3, 4, 5],
        [4, 5, 6],
        [3, 4, 5],
    ]

    frames = check_frame_limits_consistency([1, 5], dummy_frames)
    assert [4, 5] == frames


def test_convert_raw_labels_to_labels_46_4():
    np.testing.assert_array_equal([40046], convert_raw_labels_to_labels([46], 4))


def test_convert_raw_labels_to_labels_list_4():
    np.testing.assert_array_equal(
        [40046, 40057, 40002], convert_raw_labels_to_labels([46, 57, 2], 4)
    )


def test_get_image_label_10048():
    assert 48 == get_image_label(10048)


def test_strip_labels():
    labels = [10048, 20058, 10009]
    np.testing.assert_array_equal([48, 58, 9], strip_labels(labels))


def test_load_pickle_file_test_pickle():
    a = {"hello": "world"}
    data = load_pickle_file("tests/test_deformation_analysis/test.pickle")
    assert a == data


def test_generate_prefix_suffix_combination():
    prefix_list = ["abcd"]
    suffix_list = ["_1", "_2"]
    expected_output = [
        "abcd_1",
        "abcd_2",
    ]
    output = generate_prefix_suffix_combination(prefix_list, suffix_list)
    assert len(output) == len(expected_output)
    assert sorted(output) == sorted(expected_output)


def test_generate_prefix_suffix_combination_with_suffix():
    prefix_list = ["abcd_"]
    suffix_list = ["_", "*"]
    expected_output = ["abcd_"]
    output = generate_prefix_suffix_combination(prefix_list, suffix_list)
    assert len(output) == len(expected_output)
    assert sorted(output) == sorted(expected_output)
