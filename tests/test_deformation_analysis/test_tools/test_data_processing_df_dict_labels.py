import pytest
import numpy as np

from src.deformation_analysis.classes.ctj import CTJ
from src.deformation_analysis.tools.data_processing import (
    get_cell_final_label,
    get_coordinates_of_cell_labels,
    sort_ctj_list_in_cyclic_order,
    filter_pointel_labels,
    get_cell_labels,
)


@pytest.fixture(scope="session", autouse=True)
def cell_name_to_label():
    name_to_label = {
        "a7.0015_": [10046, 20042, 30042, 40042, 50042],
        "a9.0016_": [60043, 70043, 80043],
        "a9.0015_": [60044, 70044, 80044, 90044],
    }
    return name_to_label


@pytest.fixture(scope="session", autouse=True)
def properties():
    lineage = {50042: [60044, 60043]}
    names = {
        60043: "a9.0016_",
        60044: "a9.0015_",
    }
    props = {
        "cell_lineage": lineage,
        "cell_name": names,
    }
    return props


@pytest.fixture(scope="session", autouse=True)
def history():
    hist = {
        10046: [10046, 20042, 30042, 40042, 50042],
        20042: [10046, 20042, 30042, 40042, 50042],
        30042: [10046, 20042, 30042, 40042, 50042],
        40042: [10046, 20042, 30042, 40042, 50042],
        50042: [10046, 20042, 30042, 40042, 50042],
        #
        60043: [60043, 70043, 80043],
        70043: [60043, 70043, 80043],
        80043: [60043, 70043, 80043],
        #
        60044: [60044, 70044, 80044, 90044],
        70044: [60044, 70044, 80044, 90044],
        80044: [60044, 70044, 80044, 90044],
        90044: [60044, 70044, 80044, 90044],
    }
    return hist


def test_get_cell_final_astec_label(history):
    cell_name = "a7.0015_"
    cell_labels = [10046, 20042, 30042, 40042, 50042]
    cell_name_to_label_dict = {cell_name: cell_labels}

    final_cell_label = 50042
    assert final_cell_label == get_cell_final_label(
        history, cell_name_to_label_dict, cell_name
    )


def test_get_coordinates_of_cell_labels_cubic_single_cell():
    #  Cubic cell with label 2
    #
    #  Top view contact with lable 1      Bottom view contact with lable 7
    #      \   3   /                           \   3   /
    #       o-----o                             o-----o
    #       |     |                             |     |
    #     6 |  2  | 4                        6  |  2  | 4
    #       o-----o                             o-----o
    #      /   5   \                           /   5   \
    #
    # The trijunction points (represented with o) lie on the XY plane.
    pointels = {
        # top contact coords
        (1, 2, 3, 6): [[0.0, 1.0, 0.0]],
        (1, 2, 3, 4): [[1.0, 1.0, 0.0], [1.0, 1.0, 0.0]],
        (1, 2, 4, 5): [[1.0, 0.0, 0.0]],
        (1, 2, 5, 6): [[0.0, 0.0, 0.0]],
        # bottom contact coords
        (7, 2, 3, 6): [[0.0, 1.0, -1.0]],
        (7, 2, 3, 4): [[1.0, 1.0, -1.0], [1.0, 1.0, -1.0]],
        (7, 2, 4, 5): [[1.0, 0.0, -1.0]],
        (7, 2, 5, 6): [[0.0, 0.0, -1.0]],
    }
    cell_labels = [2]

    expected_coordiantes = np.asarray(
        [
            [0.0, 1.0, 0.0],
            [1.0, 1.0, 0.0],
            [1.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
        ]
    )
    expected_boundary_coords = expected_coordiantes
    expected_non_boundary_coords = []
    coordinates, boundary_coords, non_boundary_coords = get_coordinates_of_cell_labels(
        cell_labels, pointels
    )
    print(coordinates)
    np.testing.assert_array_almost_equal(expected_coordiantes, coordinates)
    np.testing.assert_array_almost_equal(expected_boundary_coords, boundary_coords)
    np.testing.assert_array_almost_equal(
        expected_non_boundary_coords, non_boundary_coords
    )


def test_get_coordinates_of_cell_labels_cubic_multi_cell():
    #  Cells with labels 8, 9, 10, 11
    #
    #      \\       3       /
    #       o------o-----o
    #       |      |  8  |
    #       |  11  o-----o
    #    6  |     /      | 4
    #       o----o   10  |
    #       | 9  |       |
    #       o----o-------o
    #      /       5      \\
    #
    pointels = {
        # Boundary cells:
        (1, 11, 3, 6): [[0.0, 1.0, 0.0]],
        (1, 11, 3, 8): [[0.6, 1.0, 0.0]],
        (1, 8, 3, 4): [[1.0, 1.0, 0.0]],
        (1, 8, 4, 10): [[1.0, 0.6, 0.0]],
        (1, 10, 4, 5): [[1.0, 0.0, 0.0]],
        (1, 10, 5, 9): [[0.4, 0.0, 0.0]],
        (1, 9, 5, 6): [[0.0, 0.0, 0.0]],
        (1, 9, 6, 11): [[0.0, 0.4, 0.0]],
        # non-boundary cells:
        (1, 8, 10, 11): [[0.6, 0.6, 0.0]],
        (1, 9, 10, 11): [[0.4, 0.4, 0.0]],
    }
    cell_labels = [8, 9, 10, 11]

    expected_coordiantes = np.asarray(
        [
            # Boundary cells:
            [0.0, 1.0, 0.0],
            [0.6, 1.0, 0.0],
            [1.0, 1.0, 0.0],
            [1.0, 0.6, 0.0],
            [1.0, 0.0, 0.0],
            [0.4, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.4, 0.0],
            # non-boundary cells:
            [0.6, 0.6, 0.0],
            [0.4, 0.4, 0.0],
        ]
    )
    expected_boundary_coords = np.asarray(
        [
            [0.0, 1.0, 0.0],
            [0.6, 1.0, 0.0],
            [1.0, 1.0, 0.0],
            [1.0, 0.6, 0.0],
            [1.0, 0.0, 0.0],
            [0.4, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.4, 0.0],
        ]
    )
    expected_non_boundary_coords = np.asarray([[0.6, 0.6, 0.0], [0.4, 0.4, 0.0]])

    coordinates, boundary_coords, non_boundary_coords = get_coordinates_of_cell_labels(
        cell_labels, pointels
    )
    print(coordinates)
    np.testing.assert_array_almost_equal(expected_coordiantes, coordinates)
    np.testing.assert_array_almost_equal(expected_boundary_coords, boundary_coords)
    np.testing.assert_array_almost_equal(
        expected_non_boundary_coords, non_boundary_coords
    )


def test_sort_ctj_list_in_cyclic_order_square_on_XY_plane():
    #  Cubic cell with label 2
    #
    #  Top view contact with lable 1
    #      \   3   /
    #       o-----o
    #       |     |
    #     6 |  2  | 4
    #       o-----o
    #      /   5   \
    #
    # The trijunction points (represented with o) lie on the XY plane.

    ctj_1 = CTJ([[0.0, 1.0, 0.0]], 2, (1, 2, 3, 6), 1)
    ctj_2 = CTJ([[1.0, 1.0, 0.0]], 2, (1, 2, 3, 4), 1)
    ctj_3 = CTJ([[1.0, 0.0, 0.0]], 2, (1, 2, 4, 5), 1)
    ctj_4 = CTJ([[0.0, 0.0, 0.0]], 2, (1, 2, 5, 6), 1)

    ctjs = [ctj_1, ctj_2, ctj_3, ctj_4]
    ctjs_cyclic = sort_ctj_list_in_cyclic_order(ctjs)

    for expected, returned in zip(ctjs, ctjs_cyclic):
        np.testing.assert_array_almost_equal(expected.coordinates, returned.coordinates)


@pytest.fixture(scope="session", autouse=True)
def pointel_label_list_example():
    pointel_label_list = [
        (1, 2, 3, 4),
        (2, 3, 4, 5),
        (3, 4, 5, 6),
        (4, 5, 6, 7),
        (5, 6, 7, 8),
        (6, 7, 8, 9),
        (7, 8, 9, 10),
        (8, 9, 10, 11),
    ]
    return pointel_label_list


def test_filter_pointel_labels_single_label(pointel_label_list_example):
    expected_list = [
        (1, 2, 3, 4),
        (2, 3, 4, 5),
        (3, 4, 5, 6),
        (4, 5, 6, 7),
    ]
    filtered_list = filter_pointel_labels(pointel_label_list_example, [4])

    assert len(filtered_list) == len(expected_list)
    assert sorted(filtered_list) == sorted(expected_list)


def test_filter_pointel_labels_multi_label_2_4(pointel_label_list_example):
    expected_list = [
        (1, 2, 3, 4),
        (2, 3, 4, 5),
        (3, 4, 5, 6),
        (4, 5, 6, 7),
    ]
    filtered_list = filter_pointel_labels(pointel_label_list_example, [2, 4])

    assert len(filtered_list) == len(expected_list)
    assert sorted(filtered_list) == sorted(expected_list)


def test_filter_pointel_labels_multi_label_2_7(pointel_label_list_example):
    expected_list = [
        (1, 2, 3, 4),
        (2, 3, 4, 5),
        (4, 5, 6, 7),
        (5, 6, 7, 8),
        (6, 7, 8, 9),
        (7, 8, 9, 10),
    ]
    filtered_list = filter_pointel_labels(pointel_label_list_example, [2, 7])
    assert len(filtered_list) == len(expected_list)
    assert sorted(filtered_list) == sorted(expected_list)


def test_filter_pointel_labels_multi_label_4_(pointel_label_list_example):
    filtered_list = filter_pointel_labels(pointel_label_list_example, [4, 8])
    assert len(filtered_list) == len(pointel_label_list_example)
    assert sorted(filtered_list) == sorted(pointel_label_list_example)


def test_get_cell_labels_label_of_cell_before_division(cell_name_to_label):
    cell_name = "a7.0015_"
    time_point = 2

    expected_labels = [20042]
    cell_labels = get_cell_labels(cell_name_to_label, cell_name, time_point, None, None)
    assert len(expected_labels) == len(cell_labels)
    assert sorted(expected_labels) == sorted(cell_labels)


def test_get_cell_labels_bad_name_to_label_dict():
    cell_name = "a7.0015_"
    bad_cell_name_to_label = {
        cell_name: [10046, 20042, 20042, 30042, 40042, 50042],
    }
    time_point = 2
    with pytest.raises(Exception) as exc_info:
        get_cell_labels(bad_cell_name_to_label, cell_name, time_point, None, None)
        assert (
            f"multiple cell labels found for {cell_name} at time point {time_point}:"
            in str(exc_info)
        )


def test_get_cell_labels_find_daughter_cells(cell_name_to_label, history, properties):
    cell_name = "a7.0015_"
    time_point = 6

    expected_labels = [60043, 60044]
    cell_labels = get_cell_labels(
        cell_name_to_label, cell_name, time_point, history, properties
    )
    assert len(expected_labels) == len(cell_labels)
    assert sorted(expected_labels) == sorted(cell_labels)


def test_get_cell_labels_time_point_before_cell_birth(
    cell_name_to_label, history, properties
):
    cell_name = "a9.0016_"
    time_point = 2

    with pytest.raises(Exception) as exc_info:
        get_cell_labels(cell_name_to_label, cell_name, time_point, history, None)
        assert (
            f"Could not find a label for {cell_name} at time point {time_point}"
            in str(exc_info)
        )
