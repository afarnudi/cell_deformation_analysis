import numpy as np
import pytest

from src.deformation_analysis.tools.geometry import (
    generate_list_of_tuples,
    convert_cartesian_to_spherical,
    rotate_coordinates_on_XY_plane,
    project_onto_best_fit_plane,
    calculate_polygon_centre,
    get_polygon_area,
    reduce_coordinates_dimension,
    convert_cartesian_to_polar,
    convert_polar_to_cartesian,
)


def test_generate_list_of_tuples():
    input_list = [[1, 2], [3, 4]]
    expected_output = [(1, 2), (3, 4)]
    output = generate_list_of_tuples(input_list)
    assert len(output) == len(expected_output)
    assert sorted(output) == sorted(expected_output)


def test_convert_cartesian_to_spherical():
    coords = np.asarray(
        [
            [0, 0, 1.0],
            [1.0, 0, 0],
            [0, 1.0, 0],
        ]
    )
    spherical_coords = np.asarray(
        [
            [1, 0, 0],
            [1, np.pi / 2, 0],
            [1, np.pi / 2, np.pi / 2],
        ]
    )
    np.testing.assert_array_almost_equal(
        spherical_coords, convert_cartesian_to_spherical(coords)
    )


def test_rotate_coordinates_on_XY_plane_xy_coords():
    xy_coords = np.asarray(
        [
            [1.0, 0.0, 0.0],
            [1.0, 1.0, 0.0],
            [0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0],
        ]
    )
    expected_output = np.asarray(
        [[0.5, -0.5, 0.0], [0.5, 0.5, 0.0], [-0.5, 0.5, 0.0], [-0.5, -0.5, 0.0]]
    )
    rotated_coords, mat = rotate_coordinates_on_XY_plane(xy_coords)
    np.testing.assert_array_almost_equal(expected_output, rotated_coords)
    np.testing.assert_array_almost_equal(np.eye(3, dtype=float), mat)


def test_rotate_coordinates_on_XY_plane_zy_coords():
    yz_coords = np.asarray(
        [
            [1.0, 0.0, 0.0],
            [1.0, 0.0, 1.0],
            [1.0, 1.0, 1.0],
            [1.0, 1.0, 0.0],
        ]
    )
    expected_coords = np.asarray(
        [[0.5, -0.5, 0.0], [-0.5, -0.5, 0.0], [-0.5, 0.5, 0.0], [0.5, 0.5, 0.0]]
    )
    expected_mat = np.asarray([[0.0, 0.0, -1.0], [0.0, 1.0, 0.0], [1.0, 0.0, 0.0]])
    rotated_coords, mat = rotate_coordinates_on_XY_plane(yz_coords)
    np.testing.assert_array_almost_equal(expected_coords, rotated_coords)
    np.testing.assert_array_almost_equal(expected_mat, mat)


def test_project_onto_best_fit_plane_xy_plane():
    xy_coords = np.asarray(
        [
            [1.0, 0.0, 0.0],
            [1.0, 1.0, 0.0],
            [0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0],
        ]
    )
    np.testing.assert_array_almost_equal(
        xy_coords, project_onto_best_fit_plane(xy_coords)
    )


def test_calculate_polygon_centre_ctj():
    coords = np.random.random(9).reshape((3, 3))
    np.testing.assert_array_almost_equal(
        np.mean(coords, axis=0), calculate_polygon_centre(coords, mode="ctj")
    )


def test_calculate_polygon_centre_polygon():
    triangle_coords = np.asarray(
        [
            [0.0, 0.0],
            [3.0, 0.0],
            [0.0, 4.0],
        ]
    )
    polygon_center = [1.0, 4.0 / 3.0]
    np.testing.assert_array_almost_equal(
        polygon_center, calculate_polygon_centre(triangle_coords, mode="shape")
    )


def test_calculate_polygon_centre_bad_mode():
    with pytest.raises(Exception) as exp_info:
        calculate_polygon_centre(None, mode="bad mode")
        assert (
            f'The polygon center calculation mode not recognised: {"bad mode"}. Please use either "ctj" or "shape".'
            in str(exp_info)
        )


def test_get_polygon_area_triangle():
    triangle_coords = np.asarray(
        [
            [0.0, 0.0],
            [3.0, 0.0],
            [0.0, 4.0],
        ]
    )
    expected_output = 6.0
    assert np.isclose(expected_output, get_polygon_area(triangle_coords))


def test_reduce_coordinates_dimension_xy_plane():
    xy_coords = np.asarray(
        [
            [1.0, 0.0, 1.0],
            [1.0, 1.0, 1.0],
            [0.0, 1.0, 1.0],
            [0.0, 0.0, 1.0],
        ]
    )
    expected_output = np.asarray([[0.5, -0.5], [0.5, 0.5], [-0.5, 0.5], [-0.5, -0.5]])
    expected_mat = np.eye(3, dtype=float)
    projected_coords, mat = reduce_coordinates_dimension(xy_coords)
    np.testing.assert_array_almost_equal(expected_output, projected_coords)
    np.testing.assert_array_almost_equal(expected_mat, mat)


def test_convert_cartesian_to_polar_1():
    circle_xy_coords = np.asarray(
        [
            [1.0, 0.0],
            [np.sqrt(2) / 2, np.sqrt(2) / 2],
            [0.0, 1.0],
            [-np.sqrt(2) / 2, np.sqrt(2) / 2],
            [-1.0, 0.0],
            [-np.sqrt(2) / 2, -np.sqrt(2) / 2],
            [0.0, -1.0],
            [np.sqrt(2) / 2, -np.sqrt(2) / 2],
        ]
    )
    expected_output = np.asarray(
        [
            [1.0, 0.0],
            [1.0, np.pi / 4],
            [1.0, np.pi / 2],
            [1.0, 3 * np.pi / 4],
            [1.0, np.pi],
            [1.0, -3 * np.pi / 4],
            [1.0, -np.pi / 2],
            [1.0, -np.pi / 4],
        ]
    )
    np.testing.assert_array_almost_equal(
        expected_output, convert_cartesian_to_polar(circle_xy_coords)
    )


def test_convert_polar_to_cartesian_1():
    expected_output = np.asarray(
        [
            [1.0, 0.0],
            [np.sqrt(2) / 2, np.sqrt(2) / 2],
            [0.0, 1.0],
            [-np.sqrt(2) / 2, np.sqrt(2) / 2],
            [-1.0, 0.0],
            [-np.sqrt(2) / 2, -np.sqrt(2) / 2],
            [0.0, -1.0],
            [np.sqrt(2) / 2, -np.sqrt(2) / 2],
        ]
    )
    circle_polar_coords = np.asarray(
        [
            [1.0, 0.0],
            [1.0, np.pi / 4],
            [1.0, np.pi / 2],
            [1.0, 3 * np.pi / 4],
            [1.0, np.pi],
            [1.0, -3 * np.pi / 4],
            [1.0, -np.pi / 2],
            [1.0, -np.pi / 4],
        ]
    )
    np.testing.assert_array_almost_equal(
        expected_output, convert_polar_to_cartesian(circle_polar_coords)
    )
