import numpy as np
import pytest

from src.deformation_analysis.classes.ctj import CTJ


@pytest.fixture(scope="session", autouse=True)
def ctj_obj():
    coords = [np.arange(9).reshape((3, 3))]
    cell_raw_label = 46
    pointel_labels = (1, 46, 48, 54)
    time_point = 2
    ctj = CTJ(coords, cell_raw_label, pointel_labels, time_point)
    return ctj


def test_ctj_init(ctj_obj):
    coords = [np.arange(9).reshape((3, 3))]

    assert 46 == ctj_obj.cell_raw_label
    assert 2 == ctj_obj.time_point
    assert 20046 == ctj_obj.cell_label
    np.testing.assert_array_equal(coords[0], ctj_obj.coordinates)
    np.testing.assert_array_equal([48, 54], ctj_obj.neighbour_raw_labels)
    np.testing.assert_array_equal([20048, 20054], ctj_obj.neighbour_labels)


def test_ctj_set_neighbour_list(ctj_obj):
    new_neighbour_labels = [48, 9]
    new_neighbour_raw_labels = [20048, 20009]
    
    ctj_obj.set_neighbour_lists(new_neighbour_labels, new_neighbour_raw_labels)

    np.testing.assert_array_equal(new_neighbour_raw_labels, ctj_obj.neighbour_raw_labels)
    np.testing.assert_array_equal(new_neighbour_labels, ctj_obj.neighbour_labels)
