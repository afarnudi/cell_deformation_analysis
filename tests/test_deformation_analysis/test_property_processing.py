from src.deformation_analysis.classes.input_data import Input_data

def test_get_cell_name_to_label_dict():
    dummy_label_to_name = {
        10001: "name_1",
        20001: "name_1",
        30001: "name_1",
        40001: "name_1",
        10002: "name_2",
        20002: "name_2",
        30002: "name_2",
        40002: "name_2",
        50002: "name_2",
        10003: "name_3",
        20003: "name_3",
        30003: "name_3",
    }
    expected_answer = {
        "name_1": [10001, 20001, 30001, 40001],
        "name_2": [10002, 20002, 30002, 40002, 50002],
        "name_3": [10003, 20003, 30003],
    }
    assert expected_answer == Input_data.get_cell_name_to_label_dict(dummy_label_to_name)


def test_cell_history_generation():
    dummy_lineage = {
        10002: [20002],
        20002: [30002],
        10003: [20003],
        20003: [30003],
        30003: [40004, 40005],
        40004: [50004],
    }
    expected_answer = {
        10002: [10002, 20002, 30002],
        20002: [10002, 20002, 30002],
        30002: [10002, 20002, 30002],
        10003: [10003, 20003, 30003],
        20003: [10003, 20003, 30003],
        30003: [10003, 20003, 30003],
        40004: [40004, 50004],
        50004: [40004, 50004],
    }
    assert expected_answer == Input_data.get_cell_history(dummy_lineage)