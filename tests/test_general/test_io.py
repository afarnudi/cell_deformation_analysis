import os
import pytest
from src.general.io import check_path_exists
from src.general.io import create_dir


def test_check_test_file():
    file_path = os.path.realpath(__file__)
    assert check_path_exists(file_path)


def test_check_test_non_existing_file():
    file_path = os.path.realpath(__file__)
    file_path += "non_existing_file"
    with pytest.raises(FileNotFoundError) as exc_info:
        check_path_exists(file_path)
        assert (
            f"Cannot locate '{file_path}'. Please check "
            "the inputs and try again." in str(exc_info)
        )

def test_create_directory(): 
    dir_name = "non_existing_dir"
    create_dir(dir_name)
    assert os.path.exists(dir_name)
    os.rmdir(dir_name) 