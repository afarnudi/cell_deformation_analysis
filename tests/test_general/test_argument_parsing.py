import sys
import copy
import pytest

from src.general.argument_parsing import (
    check_thread_value,
    create_parser,
    get_user_arguments,
    get_arguments_from_file,
)


def test_positive_int():
    assert check_thread_value(10)


def test_positive_float():
    assert check_thread_value(6546312.456)


def test_negative_int():
    with pytest.raises(RuntimeError) as exc_info:
        check_thread_value(-11)
        assert "Thread number must be a positive number." in str(exc_info)


def test_negative_float():
    with pytest.raises(RuntimeError) as exc_info:
        check_thread_value(-564.534)
        assert "Thread number must be a positive number." in str(exc_info)


def test_create_parser():
    create_parser()


def test_create_parser_text_argument_input():
    backup_sys_argv = sys.argv
    sys.argv = ["TDQ", "-a", "tests/test_general/test_inputs.txt"]
    get_user_arguments()
    sys.argv = backup_sys_argv


def test_create_parser_no_input():
    with pytest.raises(SystemExit) as exc_info:
        args = get_user_arguments()
        assert "Welcome to the Tissue Deformation Quantifier (TDQ)." in str(exc_info)


def test_user_text_input_no_file():
    dummy_input_file = "non_existing_file.txt"
    with pytest.raises(FileNotFoundError) as exc_info:
        get_arguments_from_file(dummy_input_file)
        assert (
            f"Cannot locate '{dummy_input_file}'. Please check the inputs and try again."
            in str(exc_info)
        )


def test_user_text_input_test_inputs():
    test_input = "tests/test_general/test_inputs.txt"
    expected_behaviour = [
        "cell-shape-analyser",
        "-p",
        "out/astec_8/Astec-pm8-properties.pkl",
        "-f",
        "out/nii_test/output_pointels_data_Astec-pm8_intrareg_post_t.nii.gz_1_50.pkl",
        "--mesh-map-source-axis",
        "a7.0012_",
        "a7.0012*",
        "a7.0011_",
        "a7.0016_",
        "a70016*",
        "b7.0016_",
        "-l",
        "10",
        "14",
        "a7.0016",
        "b7.0016",
    ]
    assert len(expected_behaviour) == len(get_arguments_from_file(test_input))
    assert sorted(expected_behaviour) == sorted(get_arguments_from_file(test_input))
