import pytest
import copy

from src.Astec_image_processing_tools.io import get_list_of_supported_file_extensions
from src.Astec_image_processing_tools.io import get_specified_file_numbers


@pytest.fixture(scope="session", autouse=True)
def acdq_files():
    return [
        "Empt_file_v2_without_number_t001.acdq",
        "Empt_file_v2_without_number_t002.acdq",
        "Empt_file_v2_without_number_t003.acdq",
        "Empt_file_v2_without_number_t004.acdq",
        "Empt_file_v2_without_number.acdq",
        "Empt_file_without_number_t005.acdq",
        "Empt_file_without_number_t006_v2.acdq",
        "Empt_file_without_number_t007_v001.acdq",
    ]


@pytest.fixture(scope="session", autouse=True)
def acdq_txt_files():
    return [
        "Empt_file_v2_without_number_t001.acdq",
        "Empt_file_v2_without_number_t002.acdq",
        "Empt_file_v2_without_number_t003.acdq",
        "Empt_file_v2_without_number_t004.acdq",
        "Empt_file_v2_without_number.acdq",
        "Empt_file_without_number_t005.acdq",
        "Empt_file_without_number_t006_v2.acdq",
        "Empt_file_without_number_t007_v001.acdq",
        "Empt_file_without_number_t008_v001.txt",
    ]


def test_get_list_of_supported_file_formats_acdq(acdq_files):
    path_to_files = "tests/test_Astec_image_processing_tools"
    supported_formats = "acdq"

    file_list = get_list_of_supported_file_extensions(path_to_files, supported_formats)
    assert len(file_list) == len(acdq_files)
    assert sorted(file_list) == sorted(acdq_files)


def test_get_list_of_supported_file_formats_list_acdq(acdq_files):
    path_to_files = "tests/test_Astec_image_processing_tools"
    supported_formats = ["acdq"]

    file_list = get_list_of_supported_file_extensions(path_to_files, supported_formats)
    assert len(file_list) == len(acdq_files)
    assert sorted(file_list) == sorted(acdq_files)


def test_get_list_of_supported_file_formats_txt_acdq(acdq_txt_files):
    path_to_files = "tests/test_Astec_image_processing_tools"
    supported_formats = ["acdq", "txt"]

    file_list = get_list_of_supported_file_extensions(path_to_files, supported_formats)
    assert len(file_list) == len(acdq_txt_files)
    assert sorted(file_list) == sorted(acdq_txt_files)


def test_get_specified_file_numbers_bad_numbering_no_lim(acdq_txt_files):
    file_list = get_specified_file_numbers(acdq_txt_files)
    assert len(file_list) == len(acdq_txt_files)
    assert sorted(file_list) == sorted(acdq_txt_files)


def test_get_specified_file_numbers_bad_numbering_lim_1_8(acdq_txt_files):
    file_list = get_specified_file_numbers(acdq_txt_files, [1, 8])
    expected_files = [
        "Empt_file_v2_without_number_t001.acdq",
        "Empt_file_v2_without_number_t002.acdq",
        "Empt_file_v2_without_number_t003.acdq",
        "Empt_file_v2_without_number_t004.acdq",
        "Empt_file_without_number_t005.acdq",
        "Empt_file_without_number_t006_v2.acdq",
        "Empt_file_without_number_t007_v001.acdq",
        "Empt_file_without_number_t008_v001.txt",
    ]
    assert len(file_list) == len(expected_files)
    assert sorted(file_list) == sorted(expected_files)


def test_get_specified_file_numbers_bad_numbering_lim_1_1(acdq_txt_files):
    file_list = get_specified_file_numbers(acdq_txt_files, [1, 1])
    expected_files = ["Empt_file_v2_without_number_t001.acdq"]
    assert len(file_list) == len(expected_files)
    assert sorted(file_list) == sorted(expected_files)


def test_get_specified_file_numbers_bad_numbering_lim_1_6(acdq_txt_files):
    file_list = get_specified_file_numbers(acdq_txt_files, [1, 6])
    expected_files = [
        "Empt_file_v2_without_number_t001.acdq",
        "Empt_file_v2_without_number_t002.acdq",
        "Empt_file_v2_without_number_t003.acdq",
        "Empt_file_v2_without_number_t004.acdq",
        "Empt_file_without_number_t005.acdq",
        "Empt_file_without_number_t006_v2.acdq",
    ]
    assert len(file_list) == len(expected_files)
    assert sorted(file_list) == sorted(expected_files)


def test_get_specified_file_numbers_bad_numbering_lim_3_8(acdq_txt_files):
    file_list = get_specified_file_numbers(acdq_txt_files, [3, 8])
    expected_files = [
        "Empt_file_v2_without_number_t003.acdq",
        "Empt_file_v2_without_number_t004.acdq",
        "Empt_file_without_number_t005.acdq",
        "Empt_file_without_number_t006_v2.acdq",
        "Empt_file_without_number_t007_v001.acdq",
        "Empt_file_without_number_t008_v001.txt",
    ]
    assert len(file_list) == len(expected_files)
    assert sorted(file_list) == sorted(expected_files)


def test_get_specified_file_numbers_bad_numbering_lim_2_4(acdq_txt_files):
    file_list = get_specified_file_numbers(acdq_txt_files, [2, 4])
    expected_files = [
        "Empt_file_v2_without_number_t002.acdq",
        "Empt_file_v2_without_number_t003.acdq",
        "Empt_file_v2_without_number_t004.acdq",
    ]
    assert len(file_list) == len(expected_files)
    assert sorted(file_list) == sorted(expected_files)


def test_get_specified_file_numbers_out_of_scope_numbering_10_15(acdq_txt_files):
    with pytest.raises(RuntimeError) as exc_info:
        get_specified_file_numbers(acdq_txt_files, [10, 15])
        assert (
            "There time sequences must be identified with a letter 't' before the sequence digits. "
            in str(exc_info)
        )


def test_get_specified_file_numbers_missing_images():
    list_with_duplicates = [
        "Empt_file_v2_without_number_t002.acdq",
        "Empt_file_v2_without_number_t003.acdq",
        "Empt_file_v2_without_number_t003.acdq",
        "Empt_file_v2_without_number_t004.acdq",
    ]
    with pytest.raises(RuntimeError) as exc_info:
        get_specified_file_numbers(list_with_duplicates, [2, 4])
        assert " There might be missing sequences or duplicates." in str(exc_info)
