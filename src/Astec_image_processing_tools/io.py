import os
import re


def get_list_of_supported_file_extensions(path_to_files, supported_extensions):
    """Retrieve list of files with specific extensions.

    Parameters
    ----------
    path_to_files : str
        Path to the image file directory.
    supported_extensions : list[str]
        List of supported file extensions. Example: ['.inr', '.mha.gz']

    Returns
    -------
    list[str]
        List of files with supported extensions.
    """
    if not isinstance(supported_extensions, list):
        supported_extensions = [supported_extensions]
    files = os.listdir(path_to_files)
    supported_files = []
    for ext in supported_extensions:
        for f in files:
            if f.endswith(ext):
                supported_files.append(f)
    supported_files.sort()
    return supported_files


def get_specified_file_numbers(files, frame_limits=None):
    """Get files with the numbering format '...t###...'.

    Parameters
    ----------
    files : list[str]
        List of file names.
    frame_limits : list[int], optional
        List of two integers that specifies the lower and upper limits of the file sequence required.
        Example: [4,10] which will look for seven files with the numbers 4, 5, ..., and 10 after the
        letter 't' in their names. By default None

    Returns
    -------
    list[str]
        List of file names within the frame limits.

    Raises
    ------
    RuntimeError
        If no files where found.
    RuntimeError
        If the file number sequences found have a missing time step or duplicates
    """
    if frame_limits:
        filtered_files = []
        beg_f = frame_limits[0]
        end_f = frame_limits[1]
        for file in files:
            try:
                number = re.findall("\d+", re.findall("[t]\d+", file)[0])[0]
                number = int(number)
            except:
                print(f"Skipping {file}. Could not detect the sequence digits")
                continue
            if number >= beg_f and number <= end_f:
                filtered_files.append(file)

        if len(filtered_files) == 0:
            err_msg = (
                "Something went wrong when trying to find the Astec images. "
                "Check the numbering format of the files. "
                "There time sequences must be identified with a letter 't' before the sequence digits. "
                "Example: Astec-pm8_intrareg_post_t010_v2.inr.gz\n"
            )
            raise RuntimeError(err_msg)
        elif len(filtered_files) != end_f - beg_f + 1:
            err_msg = "Something went wrong when trying to find the Astec images. There might be missing sequences or duplicates. Here are the ones I found:\n"
            for _ in filtered_files:
                err_msg += f"{_}\n"
            raise RuntimeError(err_msg)
        return filtered_files
    else:
        return files
