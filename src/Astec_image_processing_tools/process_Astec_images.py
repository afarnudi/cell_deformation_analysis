import os
import re
import pickle

from itertools import repeat
import multiprocessing as mp

from general.argument_parsing import check_thread_value
from general.io import check_path_exists
from general.io import create_dir
from Astec_image_processing_tools.io import get_list_of_supported_file_extensions
from Astec_image_processing_tools.io import get_specified_file_numbers
from Astec_image_processing_tools.Image_processing import process_image


def process_Astec_images_manager(user_args):
    """Read 3d image temporal sequences and process them for analysis.

    Parameters
    ----------
    user_args : argparse.Namespace
        Namespace with user flags and inputs.
    """
    # for path in [user_args.image_path, user_args.prop_path]:
    check_path_exists(user_args.image_path)
    check_thread_value(user_args.thread_number)
    create_dir(user_args.output_path)

    get_processed_images(
        user_args.image_path,
        user_args.output_path,
        user_args.frame_limits,
        user_args.thread_number,
    )


def generate_out_put_file_name(image_names, frame_limits=None):
    """Generate a file name adapted to the image names.

    Parameters
    ----------
    image_names : list[str]
        List of image file names.
    frame_limits : list[int], optional
        List of two integers indicating the sequence processing range (including the upper limit frame), by default None

    Returns
    -------
    str
        Output file name.
    """
    seq = re.findall("\d+", re.findall("[t]\d+", image_names[0])[0])[0]
    part_1, part_2 = image_names[0].split(seq)
    pkl_file_name = f"output_pointels_data_{part_1}{part_2}"
    if frame_limits:
        pkl_file_name += f"_{frame_limits[0]}_{frame_limits[1]}"
    else:
        seqs = seq = [
            int(re.findall("\d+", re.findall("[t]\d+", im_name)[0])[0])
            for im_name in image_names
        ]
        pkl_file_name += f"_{min(seqs)}_{max(seqs)}"
    pkl_file_name += ".pkl"
    return pkl_file_name


def check_for_existing_output(path_to_processed_data):
    """Load output data if it exists.

    Parameters
    ----------
    path_to_processed_data : str
        Path to the output data file.

    Returns
    -------
    dict
        The output data, with time steps as keys and a dictionary of cell labels and their properties as values.
    """
    if os.path.exists(path_to_processed_data):
        with open(path_to_processed_data, "rb") as f:
            try:
                all_data = pickle.load(f)
            except EOFError:
                all_data = {}
    else:
        all_data = {}
    return all_data


def get_processed_images(
    path_to_images,
    output_path="./",
    frame_limits=None,
    number_of_threads=1,
    supported_formats=[
        ".inr",
        ".inr.gz",
        ".inr.zip",
        ".lsm",
        ".mha",
        ".mha.gz",
        ".mha.zip",
        ".tif",
        ".tiff",
        ".czi",
        ".nii",
        ".nii.gz",
    ],
):
    """Process 3d temporal image sequences.

    Parameters
    ----------
    path_to_images : list[str]
        List of image file paths
    output_path : str, optional
        Save path of the output data file, by default "./"
    frame_limits : list[int], optional
        List of two integers indicating the sequence processing range (including the upper limit frame), by default None
    number_of_threads : int, optional
        number of threads used during the image processing.
    supported_formats : list[str], optional
        Timagetk supported image formats, by default [ ".inr", ".inr.gz", ".inr.zip", ".lsm", ".mha", ".mha.gz", ".mha.zip", ".tif", ".tiff", ".czi", ]

    Returns
    -------
    dict
        The output data, with time steps as keys and a dictionary of cell labels and their properties as values.
    """
    image_files = get_list_of_supported_file_extensions(
        path_to_images, supported_formats
    )
    if len(image_files) == 0:
        err_msg = (
            "Could not find images with supported formats in the input path:\n"
            f"{path_to_images}\n\n"
            f"Supported image formats:\n{supported_formats}"
        )
        raise RuntimeError(err_msg)

    image_files = get_specified_file_numbers(image_files, frame_limits)

    pkl_file_name = generate_out_put_file_name(image_files, frame_limits)
    pkl_file_path = os.path.join(output_path, pkl_file_name)
    all_data = check_for_existing_output(pkl_file_path)

    if number_of_threads > 1:
        with mp.Manager() as manager:
            mp_dict = manager.dict()
            with manager.Pool(processes=mp.cpu_count()) as pool:
                pool.starmap(
                    process_image,
                    zip(
                        repeat(mp_dict, len(image_files)),
                        repeat(path_to_images, len(image_files)),
                        image_files,
                        repeat(False, len(image_files)),
                    ),
                )
                all_data = dict(mp_dict)
                print(all_data.keys())
                with open(pkl_file_path, "wb") as fw:
                    pickle.dump(all_data, fw)
    else:
        for t, file in enumerate(image_files):
            process_image(all_data, path_to_images, file, True, pkl_file_path)

    with open(pkl_file_path, "wb") as fw:
        pickle.dump(all_data, fw)
    return all_data
