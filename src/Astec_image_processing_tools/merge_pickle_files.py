import pickle
import os

from general.io import check_path_exists


def generate_merged_output_name(input_file_list):
    out_name = "merged_"
    fname = []
    for f in input_file_list:
        temp = os.path.split(f)[1]
        for item in temp.split(".")[:-1]:
            fname.append(f"{item}_")
    fname = list(set(fname))
    fname = sorted(fname, key=len)[::-1]
    for item in fname:
        out_name += item
    out_name = out_name[:-1] + ".pkl"
    return out_name


def merge_pickle_files_manager(user_args):
    input_paths = user_args.pickle_file_paths
    for path in input_paths:
        check_path_exists(path)

    out_path, out_name = os.path.split(user_args.output_path)
    check_path_exists(out_path)
    if out_name == "":
        out_name = generate_merged_output_name(input_paths)
    else:
        if not out_name.endswith(".pkl"):
            out_name += ".pkl"

    all_data = {}
    for file in input_paths:
        with open(file, "rb") as f:
            data = pickle.load(f)
            if not isinstance(data, dict):
                raise TypeError(
                    f'The data in the "{file}" is not a dictionary. '
                    "Please make sure you are using the ACDQ process-Astec-images output."
                )

            for key in data.keys():
                try:
                    int(key)
                except:
                    err_msg = f'The key "{key}" in the "{file}" data set is not a valid key (not an int). '
                    f'Please make sure that "{file}" is a ACDQ process-Astec-images output.'
                    raise ValueError(err_msg)

            all_data.update(data)

    merged_keys = sorted(all_data.keys())
    ranged_list = list(range(min(merged_keys), max(merged_keys) + 1))
    if len(merged_keys) != len(ranged_list):
        print(
            "\n\nWarning: Inconsistency in the data time points. "
            "There may be missing time frames in the processed data. "
            "Here are the time points in the merged data set:\n\n"
            f"{merged_keys}\n\n"
        )
    with open(os.path.join(out_path, out_name), "wb") as f:
        pickle.dump(all_data, f)
        print("Data merged.")
