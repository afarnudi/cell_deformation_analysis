import re
import os
import pickle
import numpy as np

# from timagetk.io.util import POSS_EXT as TIMAGETK_FORMATS
from timagetk.io import imread
from timagetk.components.labelled_image import LabelledImage
from timagetk.algorithms.topological_elements import topological_elements_extraction3D
from nibabel.loadsave import load as nibabel_load


def process_image(
    all_data,
    path_to_images,
    image_file,
    save_progress=True,
    output_file="out_processed_image_dict.pkl",
):
    time = int(re.findall("\d+", re.findall("[t]\d+", image_file)[0])[0])
    print(f"Processing time point: {time}")  # print it

    if time in all_data:
        print(f"Already processed.")  # print it
    else:
        try:
            img = imread(os.path.join(path_to_images, image_file))
            # open the segmented image as a LabelledImage
        except:
            img = nibabel_load(os.path.join(path_to_images, image_file))
            img = np.asarray(img.dataobj)

        img_seg = LabelledImage(img, not_a_label=0)

        data = {}

        voxelsize = img_seg.get_voxelsize()
        data["voxel size"] = voxelsize

        units = img_seg.get_unit()
        data["units"] = units

        topo_elements = topological_elements_extraction3D(
            img_seg,
            elem_order=[0, 2],
            simplex=True,
            use_vq=True,
        )

        surfel_area = get_surfel_area(topo_elements[2])
        # data["surfel coordinates"] = topo_elements[2]
        data["surfel voxel count"] = surfel_area

        cell_voxel_count, cell_barycenter_coordinates_voxel = get_barycenters(img_seg)
        data["cell volume"] = cell_voxel_count
        data["cell barycenter"] = cell_barycenter_coordinates_voxel

        # data["linel coordinates"] = topo_elements[1]

        data["pointel coordinates"] = topo_elements[0]

        all_data[time] = data
        if save_progress:
            with open(output_file, "wb") as fw:
                pickle.dump(all_data, fw)


def get_barycenters(img_seg):
    """Calculate cell volumes and barycenters

    Parameters
    ----------
    img_seg : timagetk.LabelledImage
        Labeled image.

    Returns
    -------
    dict
        Cell labels (int) as key, and the number of cell voxels (int) as value.
    dict
        Cell labels (int) as key, and the barycenter of the cell voxels as value.
    """
    cell_labels = img_seg.labels()
    cell_labels = [l for l in cell_labels if l != 1]

    cell_voxel_count = {}
    cell_barycenter_coordinates_voxel = {}
    coords = img_seg.label_coordinates(
        labels=[cell_labels], real=False, axes_order="xyz"
    )
    for label in cell_labels:
        cell_voxel_count[label] = coords[label].shape[0]
        barycenter = np.mean(coords[label], axis=0)
        cell_barycenter_coordinates_voxel[label] = barycenter
    return cell_voxel_count, cell_barycenter_coordinates_voxel


def get_surfel_area(surfel_info):
    """Get the number of cell voxels in contact with the image background.

    Parameters
    ----------
    img_seg : dict
        Dictionary with tuples of two labels (ints) as key, and coordinates array as values.

    Returns
    -------
    dict
        tuples of two labels (ints) as key, and the number surface voxels (ints) as values.
    """
    surfel_area = {}
    for surfel in surfel_info:
        surfel_area[surfel] = len(surfel_info[surfel])
    return surfel_area
