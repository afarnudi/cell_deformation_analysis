import os


def check_path_exists(path):
    """_summary_

    Parameters
    ----------
    path : str
        File/Directory path.

    Returns
    -------
    bool
        True if path exists.

    Raises
    ------
    FileNotFoundError
        If file/directory was not located.
    """
    if not os.path.exists(path):
        raise FileNotFoundError(
            f"Cannot locate '{path}'. Please check the inputs and try again."
        )
    return True


def create_dir(path):
    """Create a directory if it does not exist.

    Parameters
    ----------
    path : str
        Directory path.
    """
    if not os.path.isdir(path):
        os.makedirs(path)
