def check_time_frames(frames):
    """Check user input time frames.

    Time frames is a list of two positive integer numbers where the first
    element must be smaller or than the second element.

    Parameters
    ----------
    frames : list[int]
        List of two positive integers indicating the beginning and end frame.

    Raises
    ------
    ValueError
        When any of the input time frames is negative.
    ValueError
        When the beginning time frame (first element) is larger than the
        end time frame (second element).
    """
    for f in frames:
        if f < 0:
            raise ValueError(
                f"Time frame cannot be a negative number, input time frame: {f}."
            )
    if frames[0] > frames[1]:
        raise ValueError(
            f"Time frame inconsistency start frame, {frames[0]} is larger than the end frame, {frames[1]}."
        )


def group_into_sublists(lst, sublist_size):
    """
    Group elements of a list into sublists of a specified size.

    Parameters:
    lst (list): The list to be grouped.
    sublist_size (int): The size of each sublist.

    Returns:
    list: A list of sublists, each containing `sublist_size` elements from the original list.
    """
    return [lst[i:i+sublist_size] for i in range(0, len(lst), sublist_size)]