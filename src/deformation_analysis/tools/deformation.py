import numpy as np


def get_deformation_gradient_tensor(beg_points, end_points):
    beg_points = np.asarray(beg_points)
    end_points = np.asarray(end_points)
    num_verts = beg_points.shape[0]

    beg_com = np.mean(beg_points, axis=0)
    beg_vecs = beg_points - beg_com
    mean_beg_vecs = np.mean(beg_vecs, axis=0)

    end_com = np.mean(end_points, axis=0)
    end_vecs = end_points - end_com
    mean_end_vecs = np.mean(end_vecs, axis=0)
    covarience_00 = np.zeros((2, 2))

    for vec in beg_vecs:
        covarience_00 += np.tensordot(vec, vec, axes=0) / num_verts
    covarience_00 -= np.tensordot(mean_beg_vecs, mean_beg_vecs, axes=0)

    covarience_01 = np.zeros((2, 2))
    for vec_0, vec_1 in zip(beg_vecs, end_vecs):
        covarience_01 += np.tensordot(vec_0, vec_1, axes=0) / num_verts
    covarience_01 -= np.tensordot(mean_beg_vecs, mean_end_vecs, axes=0)

    deformation_gradient_tensor = covarience_01.T @ np.linalg.inv(covarience_00)

    return deformation_gradient_tensor


def get_diff_vectors(coords):
    return coords - np.roll(coords, 1, axis=0)
