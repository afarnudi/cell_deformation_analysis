import numpy as np


def reduce_coordinates_dimension(coords):
    """
    Project a collection of points with 3D coordinates onto the best fit plane. Then take the projected coordinates and
    rotate them so that all points lie on the XY plane. The end result is a collection of points with the same order as the original with 2D coordinates.

    Parameters
    ----------
    coords : numpy.array
        N by 3 numpy array of point coordinates in the cartesian coordinate system.

    Returns
    -------
    numpy.array
        N by 2 numpy array of point coordinates in the cartesian coordinate system.
    numpy.array
        3 by 3 rotation matrix used to rotate the coordinates onto the XY plane.
    """
    projected_coords = project_onto_best_fit_plane(coords)
    rotated_coords, mat = rotate_coordinates_on_XY_plane(projected_coords)

    return rotated_coords[:, :2], mat


def generate_list_of_tuples(my_list):
    """
    Convert a list of lists to a list of tuples.

    Parameters
    ----------
    my_list : list
        A list of lists.

    Returns
    -------
    list
        A list of tuples
    """
    return [tuple(_) for _ in my_list]


def convert_cartesian_to_spherical(xyz):
    """
    Convert cartesian coordinates to spherical coordinates.

    Parameters
    ----------
    xyz : numpy.array
        N by 3 numpy array of cartesian coordinates.

    Returns
    -------
    numpy.array
        N by 3 numpy array of spherical coordinates. Columns correspond to r, theta (angle with the z axis), and phi (projected angle with the X axis on the XY plane)
    """
    ptsnew = np.zeros_like(xyz)
    xy = xyz[:, 0] ** 2 + xyz[:, 1] ** 2
    ptsnew[:, 0] = np.sqrt(xy + xyz[:, 2] ** 2)
    ptsnew[:, 1] = np.arctan2(
        np.sqrt(xy), xyz[:, 2]
    )  # for elevation angle defined from Z-axis down
    # ptsnew[:,4] = np.arctan2(xyz[:,2], np.sqrt(xy)) # for elevation angle defined from XY-plane up
    ptsnew[:, 2] = np.arctan2(xyz[:, 1], xyz[:, 0])
    return ptsnew


def rotate_coordinates_on_XY_plane(coords):
    """
    Rotate input coordinates on the xy plane, with the center of coordinates translated to the origin.
    The input coordinates must already lie on a plane.

    Parameters
    ----------
    coords : numpy.array
        A N by 3 dimensional numpy.array where N is the number of points.

    Returns
    -------
    numpy.array
        N by 3 dimensional numpy array of point that lie on the xy plane and their geometrical centre located at the origin.
    numpy.array
        3 by 3 rotation matrix used to rotate coordinates and align with the xy plane.
    """
    z_direction = np.asarray([0.0, 0.0, 1.0])
    centre = calculate_polygon_centre(coords)
    u_norm = get_normal_vector_to_best_fit_plane(coords)
    if np.array_equal(u_norm, z_direction) or np.array_equal(u_norm, -z_direction):
        return coords - centre, np.eye(3, dtype=float)
    if np.dot(u_norm, centre) < 0:
        u_norm *= -1

    mat = rotation_matrix_from_vectors(u_norm, z_direction)
    rotated_coords = (mat @ (coords - centre).T).T
    return rotated_coords, mat


def rotation_matrix_from_vectors(vec1, vec2):
    """
    Find the rotation matrix that aligns vec1 to vec2

    Parameters
    ----------
    vec1 : numpy.array
        3 dimensional vector selected as "source".
    vec2 : numpy.array
        3 dimensional vector selected as "destination".

    Returns
    -------
    numpy.array
        A transform matrix (3x3) which when applied to vec1, aligns it with vec2.
    """
    a, b = (vec1 / np.linalg.norm(vec1)).reshape(3), (
        vec2 / np.linalg.norm(vec2)
    ).reshape(3)
    v = np.cross(a, b)
    c = np.dot(a, b)
    s = np.linalg.norm(v)
    kmat = np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    rotation_matrix = np.eye(3) + kmat + kmat.dot(kmat) * ((1 - c) / (s**2))
    return rotation_matrix


def project_onto_best_fit_plane(coords):
    """
    Fit a plane to coordinates using linear regression and project the coordinates onto the plane.

    Parameters
    ----------
    coords : numpy.array
        N by 3 numpy array of point coordinates in the cartesian coordinate system.

    Returns
    -------
    numpy.array
        N by 3 numpy array of point coordinates in the cartesian coordinate system that lie on the same plane.
    """
    centre = calculate_polygon_centre(coords)
    u_norm = get_normal_vector_to_best_fit_plane(coords)
    projected_coords = np.asarray(
        [coord - np.dot(coord - centre, u_norm) * u_norm for coord in coords]
    )
    return projected_coords


def get_normal_vector_to_best_fit_plane(coords):
    """
    Get the normal vector For a collection of points that lie on the same plane.
    Take the cartesian coordinates of points, fit a plane to them, and calculate the normal vector to the plane.

    Parameters
    ----------
    coords : numpy.array
        N by 3 numpy array of point coordinates in the cartesian coordinate system.

    Returns
    -------
    numpy.array
        1 dimensional array of length 3 describing the normal vector.
    """
    centre = calculate_polygon_centre(coords)
    u, s, vh = np.linalg.svd(coords - centre)
    u_norm = vh[2, :]
    return u_norm


def calculate_polygon_centre(vertices, mode="ctj"):
    """
    calculate the cartesian coordinates of the centre of a polygon.

    Parameters
    ----------
    vertices : numpy.array
        N by 3 array of point coordinates in the cartesian coordinate system. Or N by 2 array of polygon vertices.
    mode : str, optional
        The method used to calculate the polygon center. The 'ctj' method calculates the mean of the coordinates along each axis.
        The 'shape' method calculated the centroid of a non-self-intersecting closed polygon (
        more info at https://en.wikipedia.org/wiki/Centroid#Of_a_polygon). By default "ctj"

    Returns
    -------
    numpy.array
        1 dimensional array of length 3 (2 in case of vertices has polygon coordinates), describing the coordinates of the center of the vertices.

    Raises
    ------
    Exception
        When input mode is not one of the defined choices: 'ctj' or 'shape'
    """
    if mode == "ctj":
        center = np.mean(vertices, axis=0)
    elif mode == "shape":
        center = get_polygon_centroid(vertices)
    else:
        raise Exception(
            f'The polygon center calculation mode not recognised: {mode}. Please use either "ctj" or "shape".'
        )
    return center


def get_polygon_centroid(x_y):
    """
    Calculate the centroid of a non-self-intersecting closed polygon (more info at https://en.wikipedia.org/wiki/Centroid#Of_a_polygon)
    Code implementation taken from stackoverflow:
    https://stackoverflow.com/questions/75699024/finding-the-centroid-of-a-polygon-in-python
    Parameters
    ----------
    x_y : numpy.array
        The vertex coordinates of a 2D polygon.

    Returns
    -------
    numpy.array
        1 dimensional array of length 2.
    """
    # https://stackoverflow.com/questions/75699024/finding-the-centroid-of-a-polygon-in-python
    # Same polygon, but with vertices cycled around. Now the polygon
    # decomposes into triangles of the form origin-polygon[i]-polygon2[i]
    polygon2 = np.roll(x_y, -1, axis=0)

    # Compute signed area of each triangle
    signed_areas = 0.5 * np.cross(x_y, polygon2)

    # Compute centroid of each triangle
    centroids = (x_y + polygon2) / 3.0

    # Get average of those centroids, weighted by the signed areas.
    centroid = np.average(centroids, axis=0, weights=signed_areas)
    return centroid


def get_polygon_area(x_y):
    """
    Use the shoelace algorithm to calculate the area of a polygon.

    Parameters
    ----------
    x_y : numpy.array
        The vertex coordinates of a 2D polygon.

    Returns
    -------
    float
        The area of the polygon
    """
    x_y = np.array(x_y)
    # if x_y.shape[1] != 2:
    #     x_y = x_y.reshape(-1, 2)

    x = x_y[:, 0]
    y = x_y[:, 1]

    S1 = np.sum(x * np.roll(y, -1))
    S2 = np.sum(y * np.roll(x, -1))

    area = 0.5 * np.absolute(S1 - S2)

    return area


def convert_cartesian_to_polar(coords):
    """
    Convert 2D cartesian coordinates to polar coordinates.

    Parameters
    ----------
    coords : numpy.array
        N by 2 cartesian coordinates.

    Returns
    -------
    numpy.array
        N by 2 polar coordinates where columns correspond to the radius and the angle respectively.
    """
    polar = np.zeros_like(coords)
    if len(polar.shape) == 1:
        polar[0] = np.sqrt(np.sum(np.square(coords)))
        polar[1] = np.arctan2(coords[1], coords[0])
    else:
        polar[:, 0] = np.sqrt(np.sum(np.square(coords), axis=1))
        polar[:, 1] = np.arctan2(coords[:, 1], coords[:, 0])
    return polar


def convert_polar_to_cartesian(polar):
    """
    Convert polar coordinates to cartesian coordinates.

    Parameters
    ----------
    polar : numpy.array
        N by 2 polar coordinates where columns correspond to the radius and the angle respectively.

    Returns
    -------
    numpy.array
        N by 2 cartesian coordinates.
    """
    coords = np.zeros_like(polar)
    if len(coords.shape) == 1:
        coords[0] = polar[0] * np.cos(polar[1])
        coords[1] = polar[0] * np.sin(polar[1])
    else:
        coords[:, 0] = polar[:, 0] * np.cos(polar[:, 1])
        coords[:, 1] = polar[:, 0] * np.sin(polar[:, 1])
    return coords


def get_checkerboard_filter(
    coords, resolution_ax_1, type="uniform", resolution_ax_2=None
):
    """
    Generate a boolean array for an array of coordinates where the boolean array's
    elements filter the coordinates of the input array to show a checkerboard pattern.

    Parameters
    ----------
    coords : numpy.array
        N by 2 dimensional array of coordinates.
    resolution_ax_1 : float
        The width of the checkerboard patter in the 0 axis direction.
    type : str, optional
        Select the generated checkerboard pattern based on the input coordinates. Choices: uniform,
        polar norm, or polar. By default "uniform".
        If 'uniform', the output checkerboard filter will have rectangles with width resolution_ax_1
        in the 0 axis direction and resolution_ax_2 in the 1 axis.
        If 'polar', the output checkerboard filter will have polar sectors with resolution_ax_1
        edge (radial) length, and resolution_ax_2 arc length .
        If 'polar norm', the output checkerboard filter will have polar sectors with resolution_ax_2
        arc length and the edge length (radial) will be adjusted so that all
        sectors have resolution_ax_1 surface area.
    resolution_ax_2 : float, optional
        The width of the checkerboard pattern in the second axis direction. If None, the input value
        of the resolution_ax_1 will be used for a 'uniform' pattern and pi/3 for both 'polar'
        and 'polar norm' patterns. By default None

    Returns
    -------
    numpy.array
        Numpy boolean array with the same dimension as the input coordinates that filters the input
        coordinates with a checkerboard pattern.
    """
    if type == "uniform":
        if resolution_ax_2 is None:
            resolution_ax_2 = resolution_ax_1
        checkerboard = (np.abs(np.floor(coords[:, 0] / resolution_ax_1)) % 2 == 0) == (
            np.abs(np.floor(coords[:, 1] / resolution_ax_2)) % 2 == 0
        )
    if type == "polar norm":
        if resolution_ax_2 is None:
            resolution_ax_2 = np.pi / 3
        n_max = int(max(coords[:, 0]) ** 2 // resolution_ax_1)
        checkerboard = np.zeros_like(coords[:, 0], dtype=bool)
        for n in range(0, n_max, 2):
            min_lim = np.sqrt(n * resolution_ax_1)
            max_lim = np.sqrt((n + 1) * resolution_ax_1)
            checkerboard += (coords[:, 0] >= min_lim) & (coords[:, 0] < max_lim)
        checkerboard = checkerboard == (
            np.abs(np.floor(coords[:, 1] / resolution_ax_2)) % 2 == 0
        )
    if type == "polar":
        if resolution_ax_2 is None:
            resolution_ax_2 = np.pi / 3
        checkerboard = (np.abs(np.floor(coords[:, 0] / resolution_ax_1)) % 2 == 0) == (
            np.abs(np.floor(coords[:, 1] / resolution_ax_2)) % 2 == 0
        )
    return checkerboard
