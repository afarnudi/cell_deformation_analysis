import numpy as np
import matplotlib.pyplot as plt
import sys


from deformation_analysis.tools.geometry import reduce_coordinates_dimension

def plot_scatter(coords, ax, color, s=10):
    ax.scatter(coords[:, 0], coords[:, 1], coords[:, 2], c=color, s=s)



def visualise_ellipse_deformation(
    coordinates_in_time,
    coordinates,
    eigenvalues,
    eigenvectors,
    axes=None,
):
    analysis_dimension = len(eigenvalues)
    manage_figure = False
    if axes is None:
        fig, axes = plt.subplots(
            nrows=1,
            ncols=3,
            figsize=(16, 8),
            subplot_kw=dict(projection="3d", aspect="equal"),
        )
        manage_figure = True

    beg_coords = coordinates_in_time[0]
    end_coords = coordinates_in_time[-1]
    _, rotation_matrix_beg = reduce_coordinates_dimension(
        beg_coords
    )
    plot_scatter(beg_coords, axes[0], "r")
    plot_scatter(np.asarray(coordinates), axes[-1], "r")
    plot_scatter(end_coords, axes[-1], "k", s=40)

    for ax, coords, ls, c in zip(
        [axes[0], axes[0], axes[-1]],
        [beg_coords, end_coords, end_coords],
        ["-", "--", "--"],
        ["b", "orange", "orange"],
    ):
        extended_coords = coords.copy()
        extended_coords = np.concatenate((extended_coords, [coords[0]]), axis=0)
        ax.plot(
            extended_coords[:, 0],
            extended_coords[:, 1],
            extended_coords[:, 2],
            c=c,
            ls=ls,
        )

    for ax, coords, ls, c in zip(
        [axes[1], axes[1]],
        [beg_coords, end_coords],
        ["-", "--"],
        ["b", "orange"],
    ):
        extended_coords = coords.copy()
        extended_coords = np.concatenate((extended_coords, [coords[0]]), axis=0)
        ax.plot(
            extended_coords[:, 0],
            extended_coords[:, 1],
            extended_coords[:, 2],
            c=c,
            ls=ls,
        )
    
    # plot Eigenvectors and Eigenvalues
    ax = axes[0]
    centre = np.mean(beg_coords, axis=0)
    for i in range(analysis_dimension):
        if analysis_dimension == 3:
            vectors = np.asarray([eigenvectors[:, i], -eigenvectors[:, i]]).reshape(
                (2, 3)
            )
        elif analysis_dimension == 2:
            vectors = np.asarray(
                [
                    rotation_matrix_beg.T @ [eigenvectors[0, i], eigenvectors[1, i], 0],
                    -rotation_matrix_beg.T
                    @ [eigenvectors[0, i], eigenvectors[1, i], 0],
                ]
            ).reshape((2, 3))
        coords = centre + 10 * np.sqrt(eigenvalues[i]) * vectors
        if eigenvalues[i] < 0.95:
            c = "orange"
        elif eigenvalues[i] > 1.05:
            c = "green"
        else:
            c = "b"
        ax.plot(
            coords[:, 0],
            coords[:, 1],
            coords[:, 2],
            lw=4,
            c=c,
            label=rf"$\lambda_{{{i+1}}}={{{eigenvalues[i]:2.2}}}$",
        )
        print(eigenvalues[i])

    ax = axes[1]
    coordinates_in_time = np.asarray(coordinates_in_time)
    for i in range(coordinates_in_time.shape[1]):
        ax.plot(
            coordinates_in_time[:, i, 0],
            coordinates_in_time[:, i, 1],
            coordinates_in_time[:, i, 2],
            ".-",
        )
    for coords, c in zip([beg_coords, end_coords], ["g", "b"]):
        ax.scatter(
            coords[:, 0],
            coords[:, 1],
            coords[:, 2],
            c=c,
        )

    for ax in axes:
        ax.set_xlabel("X Label")
        ax.set_ylabel("Y Label")
        ax.set_zlabel("Z Label")
        ax.axis("equal")
        ax.set_box_aspect([1, 1, 1])
        for dim, func in zip(range(3), [ax.set_xlim, ax.set_ylim, ax.set_zlim]):
            xs = coordinates_in_time[:, :, dim]
            xmin = np.min(xs)
            xmax = np.max(xs)
            delta = np.abs(xmax - xmin)
            xmin = xmin - delta * 0.05
            xmax = xmax + delta * 0.05
            func(xmin, xmax)
        ax.axis("equal")
        ax.set_box_aspect([1, 1, 1])

    if manage_figure:
        axes[0].legend()
        fig.tight_layout()
        plt.show()
        sys.exit()