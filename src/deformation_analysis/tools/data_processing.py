import pickle
import numpy as np

from deformation_analysis.classes.ctj import CTJ
from deformation_analysis.tools.geometry import (
    project_onto_best_fit_plane,
    rotate_coordinates_on_XY_plane,
    convert_cartesian_to_spherical,
    generate_list_of_tuples,
)


def load_pickle_file(file_path):
    """Open pickle file in binary mode and load object.

    Parameters
    ----------
    file_path : str
        Path to pickle file

    Returns
    -------
    object
        Object in the pickled representation data of the file.
    """
    with open(file_path, "rb") as f:
        data = pickle.load(f)
    return data


def get_cells_label_list(cell_names, cell_name_to_label):
    """Generate a two dimensional list of the cell labels of each cell_name

    Parameters
    ----------
    cell_names : list[str]
        List of cell names
    cell_name_to_label : dict
        Dictionary with cell names as keys and list of associated cell labels as values.

    Returns
    -------
    list
        Two dimensional list of cell labels.
    """
    return [cell_name_to_label[cell] for cell in cell_names]


def convert_labels_list_to_time_frames(cells_label_list):
    """Convert a two dimensional list of cell labels to a list with same dimensions where the elements are the time frame of each label.

    Parameters
    ----------
    cells_label_list : list[list[int]]
        Two dimensional list of cell labels.

    Returns
    -------
    list[list[int]]
        Two dimensional list of cell time frames.
    """
    cell_frames = []
    for label_list in cells_label_list:
        cell_frames.append(list(map(get_label_time, label_list)))
    return cell_frames


def check_frame_limits_consistency(input_frames, cell_frames_list):
    """Check if input time frame is consistent with cell life time in the cell frames list

    Parameters
    ----------
    input_frames : list[int]
        list of two integers indicating the beginning and end of analysis frames.
    cell_frames_list : _type_
        _description_

    Returns
    -------
    _type_
        _description_

    Raises
    ------
    RuntimeError
        _description_
    RuntimeError
        _description_
    """
    min_frames = [min(frame_list) for frame_list in cell_frames_list]
    max_frames = [max(frame_list) for frame_list in cell_frames_list]

    best_min_frame = max(min_frames)
    best_max_frame = min(max_frames)

    if best_max_frame < best_min_frame:
        raise RuntimeError(
            "Could not find an image frame where all the selected cells appear at the same time.\n"
            "Please check the list of cell names and try agin."
        )

    if input_frames[0] < best_min_frame:
        if input_frames[1] < best_min_frame:
            raise RuntimeError(
                "The first image frame where all selected cells appear at the same time is "
                f"{best_min_frame} which is outside the input analysis frame limits: "
                f"{input_frames[0]}, {input_frames[1]}.\n\nPlease check the images and adjust "
                "the frame limits or cell names"
            )
        print(
            "Warning: The first image where all cells appear at the same time is frame "
            f"{best_min_frame}.\n Will change the analysis frame limits to {best_min_frame}, {input_frames[1]}"
        )
        input_frames[0] = best_min_frame
    return input_frames


def get_coordinates_of_cell_labels(cell_labels, pointels):
    """
    Find cellular trijunction points (CTJ) coordinates of a cell or a group of cells.
    The CTJs are pointels that contain the label of the cell, the label of the background,
    and two other cell labels.

    Example I: Assume we have a cubic cell with label 2. The top of the cell is in contact with
    the background (labeled 1), and the bottom is in contact with a large cell labeled 7:

                 1
               ________
             /        /|
            /_______ / |
            |        | |
            |    2   | |
            |________|/

                7

    We can assume the top side of the cube lies on the Z=0 plane and the bottom is on the Z=-1 plane.
    All voxels in the z>0 space are background voxels and all voxels with coordinates z<-1 are a part of
    the big cell with label 7. The cubic cell has neighbours that are defined between these planes:

      Top view: looking from a point in z>0 in the direction of -z

          \\   3   /
           o-----o
           |     |
         6 |  2  | 4
           o-----o
          /   5   \\

    In the picture above the CTJs are represented with the 'o' symbol. The coordinates of the CTJ in the
    top layer in pointel form are defined as:

    (1, 2, 3, 6): [[0, 1, 0]]
    (1, 2, 3, 4): [[1, 1, 0]]
    (1, 2, 4, 5): [[1, 0, 0]]
    (1, 2, 5, 6): [[0, 0, 0]]

    Where tuple on the left define the CTJ surrounding labels and the list on the right contains 3D coordinate
    lists associated with this point. Due to imaging and labeling complexities, sometimes multiple coordinates
    may be derived for a single CTJ, hence a list of derived coordinates.

    The bottom CTJ pointels have similar label list but instead of the background they are in contact with cell
    label 7 and the coordinates lie on the z=-1 plane:

    (7, 2, 3, 6): [[0, 1, -1]]
    (7, 2, 3, 4): [[1, 1, -1]]
    (7, 2, 4, 5): [[1, 0, -1]]
    (7, 2, 5, 6): [[0, 0, -1]]

    For this case,
    coordinates: Contain the 4 coordinates corresponding to the top CTJs.
    boundary_coords:  Contain the same list as coordinates.
    non_boundary_coords: Is an empty list.


    Example 2: Lets assume now that cell label 2 has gone through two successive divisions, resulting in cells
    with labels 8, 9, 10, 11. with the following arrangement

         \\       3       /
           o------o-----o
           |      |  8  |
           |  11  o-----o
        6  |     /      | 4
           o----o   10  |
           | 9  |       |
           o----o-------o
          /       5      \\

    In case cell_labels = [8,9,10,11] the returned lists are:
    coordinates: Contain all 10 visible CTJs in the image above.
    boundary_coords: Contains Only the 8 CTJs that lie on the boundary of the cell group:
        (1, 11, 3, 6)     (1, 11, 3, 8)    (1, 8, 3, 4)
        (1, 9, 6, 11)                      (1, 8, 4, 10)
        (1, 9, 5, 6)      (1, 10, 5, 9)    (1, 10, 4, 5)
    non_boundary_coords: contains the coordinates of the two CTjs that are defined between the cell group:
        (1, 8, 10, 11)
        (1, 9, 10, 11)

    Parameters
    ----------
    cell_labels : list
        The list of cell labels (int.
    pointels : dict
        The pointel dictionary with tuple of cell labels as keys and list of coordinates as values.

    Returns
    -------
    numpy.array
        coordinates: Array of all CTJ coordinates defined between cell group members and the background label.
    numpy.array
        boundary_coords: Array of all CTJ coordinates defined between cell group members, the background label
        and neighbouring cell labels.
    numpy.array
        Array of all CTJ coordinates defined ONLY between cell group members and the background label.
    """
    background_label = 1
    cell_labels_and_background = np.append(cell_labels, background_label)

    coordinates = []
    boundary_coords = []
    non_boundary_coords = []

    disjoint_labels = []
    for label_list, coords in pointels.items():
        if background_label in label_list:
            if not set(cell_labels).isdisjoint(label_list):
                coordinates.append(np.mean(coords, axis=0))
                disjoint_set = set(label_list) - set(cell_labels_and_background)
                if len(disjoint_set) != 0:
                    disjoint_labels.extend(disjoint_set)
                    boundary_coords.append(np.mean(coords, axis=0))
                else:
                    non_boundary_coords.append(np.mean(coords, axis=0))

    return coordinates, boundary_coords, non_boundary_coords


def convert_raw_labels_to_labels(cell_raw_labels, time_point):
    return [cell_raw_label + time_point * 10000 for cell_raw_label in cell_raw_labels]


def sort_ctj_list_in_cyclic_order(ctjs):
    """
    Order the list of cell trijunction objects and put them in a cyclic order.

    Take the coordinates of the CTJs and project them onto a best-fit plane.
    Sort the CTJs based on the coordinates on the projected plane.

    Parameters
    ----------
    ctjs : list
        List of CTJ objects.

    Returns
    -------
    list
        List of cyclic ordered CTJ objects.
    """
    coords = [point.coordinates for point in ctjs]

    projected_coords = project_onto_best_fit_plane(coords)
    rotated_coords, mat = rotate_coordinates_on_XY_plane(projected_coords)
    rotated_coords_spherical = convert_cartesian_to_spherical(rotated_coords)

    rotated_coords_spherical = generate_list_of_tuples(rotated_coords_spherical)

    sorted_indices = [
        x
        for _, x in sorted(
            zip(rotated_coords_spherical, range(len(coords))),
            key=lambda pair: pair[0][-1],
        )
    ][::-1]

    ctjs_cyclic = [ctjs[ind] for ind in sorted_indices]
    return ctjs_cyclic


def get_image_label(label):
    return label % 10_000


def strip_labels(cell_labels):
    return list(map(get_image_label, cell_labels))


def filter_pointel_labels(pointel_label_lists, filter_list):
    """
    get the list of pointels that have at least one member in both pointel_label_lists and filter_list.

    Parameters
    ----------
    pointel_label_lists : list
        list of tuples. Example: [(1,2,3,5)]
    filter_list : list
        List of labels (the same type as the content of tuple in the pointel_label_lists).

    Returns
    -------
    list
        List of pointel labels (tuples of labels).
    """
    filtered_pointels = []
    for pointel_labels in pointel_label_lists:
        if set(pointel_labels) & set(filter_list):
            filtered_pointels.append(pointel_labels)
    return filtered_pointels


def get_cell_final_label(history, cell_name_to_label, cell_name):
    arbitrary_cell_label = cell_name_to_label[cell_name][0]
    final_cell_label = history[arbitrary_cell_label][-1]
    return final_cell_label


def get_cell_labels(cell_name_to_label, cell_name, time_point, history, properties):
    """
    Recursively retrieve cell labels for a specific time.

    For an undivided cell, return the cell label at the target time point. If a cell has already divided
    at the target time point, return the daughter cell labels. This procedure repeats recursively for
    every daughter cell if they have already divided before the target time point.

    Parameters
    ----------
    cell_name_to_label : dict
        Dictionary with cell names as keys and a sorted list of all cell labels as values.
    cell_name : str
        Cell name.
    time_point : int
        Target time point
    history : dict
        Dictionary with cell labels as values, and list of all corresponding cell labels as values.
    properties : dict
        Dictionary containing the "cell_lineage" and "cell_name" keys.
        The value of the "cell_lineage" is a dictionary with the cell label as key and a list of the
        succeeding cell or, in case of division, cells as values.
        The value of the "cell_name" is a dictionary with cell labels as keys and corresponding cell names
        as values.

    Returns
    -------
    List
        list of the cell label(s) at the target time point.

    Raises
    ------
    Exception
        When the cell_name_to_label dictionary contains multiple labels for the same time point.
    Exception
        The cell label does not exist at the time. For example if the cell was not born at the
        target time.
    """
    cell_labels = np.asarray(cell_name_to_label[cell_name])

    labels_of_interest = cell_labels[cell_labels // 10_000 == time_point]

    if len(labels_of_interest) == 1:
        return labels_of_interest
    elif len(labels_of_interest) > 1:
        raise Exception(
            f"multiple cell labels found for {cell_name} at time point {time_point}:\n{labels_of_interest}"
        )
    elif labels_of_interest.size == 0:
        cell_final_label = get_cell_final_label(history, cell_name_to_label, cell_name)
        cell_final_time_point = get_label_time(cell_final_label)

        if cell_final_time_point < time_point:
            new_labels = []
            daughters = properties["cell_lineage"][cell_final_label]

            for cell in daughters:
                cell_daughter_name = properties["cell_name"][cell]
                cell_labels = get_cell_labels(
                    cell_name_to_label,
                    cell_daughter_name,
                    time_point,
                    history,
                    properties,
                )
                new_labels.extend(cell_labels)

            return new_labels
        else:
            raise Exception(
                f"Could not find a label for {cell_name} at time point {time_point}"
            )
    return []


def get_label_time(label):
    return label // 10000


def generate_prefix_suffix_combination(prefix_list, suffix_list):
    combinations = []
    for prefix in prefix_list:
        if prefix[-1] not in suffix_list:
            for suf in suffix_list:
                combinations.append(prefix + suf)
        else:
            combinations.append(prefix)
    return combinations


def check_cell_names(cell_names, cell_name_to_label):
    """
    Make sure all cell names exist in the "cell_name_to_label" dictionary.

    Parameters
    ----------
    cell_names : list of str
        list of cell names.
    cell_name_to_label : dict
        Dictionary with string cell names as keys and list of all labels (int) associated with a cell name.

    Raises
    ------
    RuntimeError
        When a cell name is not in the "cell_name_to_label" keys.
    """
    for cell_name in cell_names:
        if cell_name not in cell_name_to_label.keys():
            raise RuntimeError(
                f'"{cell_name}" is not a valid cell name. Please check the cell name inputs and try again.'
            )