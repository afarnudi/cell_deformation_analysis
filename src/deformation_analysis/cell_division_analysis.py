import numpy as np
import matplotlib.pyplot as plt

from deformation_analysis.classes.ctj import CTJ
from deformation_analysis.classes.mesh import Mesh
from deformation_analysis.classes.input_data import Input_data
from deformation_analysis.tools.data_processing import (
    generate_prefix_suffix_combination,
    check_cell_names,
    get_cells_label_list,
    convert_labels_list_to_time_frames,
    get_label_time,
    strip_labels,
    filter_pointel_labels,
    sort_ctj_list_in_cyclic_order,
)
from general.io import check_path_exists
from deformation_analysis.tools.general import group_into_sublists
from deformation_analysis.cell_shape_analysis import (
    generate_logmap,
    get_closest_coordinates_index,
    calculate_perpendicular_vector,
)


def find_common_time_frame_with_all_daughter_cells(cell_frames):
    """
    Finds the set of time frames where the daughter cells of all input cells coexist at the same time.

    Parameters
    ----------
    cell_frames : list of list of int
        A two-dimensional list representing the time frames of daughter cells for each input cell.

    Returns
    -------
    list of int
        A list containing the time frames where daughter cells of all input cells coexist.

    Raises
    ------
    RuntimeError
        If there is no common time frame where the daughter cells of all input cells coexist.
    """
    common_set = set(cell_frames[0])
    for row in cell_frames[1:]:
        common_set &= set(row)
    if len(common_set) == 0:
        raise RuntimeError(
            f"Cannot find a time frame where the daughter cells of all input cells coexist at the same time."
        )
    return list(common_set)


def get_daughter_names(cell_names, input_data):
    """
    Retrieves the names of daughter cells given the names of parent cells.

    Parameters
    ----------
    cell_names : list of str
        A list containing the names of parent cells for which daughter cell names are to be retrieved.

    input_data : Input_data
        An object containing cell labels and lineage information.

    Returns
    -------
    list of str
        A list containing the names of daughter cells corresponding to the input parent cell names.

    Notes
    -----
    This function assumes that the input_data object has the following attributes:
    - cell_name_to_label: A dictionary mapping cell names to their corresponding labels.
    - properties: A dictionary containing various properties of cells, including "cell_lineage",
                  which is a dictionary mapping cell labels to lists of daughter cell labels.

    Raises
    ------
    KeyError
        If the input_data object does not contain the necessary attributes.
    """
    daughter_cell_names = []
    for cell_name in cell_names:
        cell_label_before_division = input_data.cell_name_to_label[cell_name][-1]
        daughter_labels = input_data.properties["cell_lineage"][
            cell_label_before_division
        ]
        for label in daughter_labels:
            daughter_cell_names.append(input_data.cell_label_to_name[label])
    return daughter_cell_names


class LabelNotFoundError(Exception):
    """Exception raised when a label is not found for the given name and time."""

    pass


def get_cell_label_at_time(name, name_to_label_dict, time):
    """
    Get the cell label associated with a given name at a specific time.

    Parameters
    ----------
    name : str
        The name of the cell.
    name_to_label_dict : dict
        A dictionary mapping cell names to their labels.
    time : int
        The time at which to retrieve the cell label.

    Returns
    -------
    str
        The label associated with the cell at the specified time.

    Raises
    ------
    LabelNotFoundError
        If no label is found for the given name and time.
    """
    labels = name_to_label_dict.get(name, [])
    for label in labels:
        if time == get_label_time(label):
            return label
    raise LabelNotFoundError(f"No label found for '{name}' at time {time}")


def cell_division_analysis(user_args):
    cell_names = user_args.cell_names
    cell_names = generate_prefix_suffix_combination(cell_names, ["_", "*"])

    data_file_path = user_args.processed_file_path
    property_file_path = user_args.property_file_path
    check_path_exists(data_file_path)
    check_path_exists(property_file_path)

    input_data = Input_data(data_file_path, property_file_path)
    check_cell_names(cell_names, input_data.cell_name_to_label)

    check_cell_names(user_args.mesh_map_source_axis, input_data.cell_name_to_label)
    origin_ctj_cell_names = user_args.mesh_map_source_axis[:3]
    v_axis_ctj_cell_names = user_args.mesh_map_source_axis[3:]

    daughter_cell_names = get_daughter_names(cell_names, input_data)
    cells_label_list = get_cells_label_list(
        daughter_cell_names, input_data.cell_name_to_label
    )
    cell_frames = convert_labels_list_to_time_frames(cells_label_list)
    common_time_frame = min(find_common_time_frame_with_all_daughter_cells(cell_frames))
    print(common_time_frame)
    mesh = Mesh(input_data.embryo_path.format(common_time_frame))
    logmap = generate_logmap(
        common_time_frame,
        origin_ctj_cell_names,
        v_axis_ctj_cell_names,
        input_data,
        mesh,
    )
    fig, axes = plt.subplots(
        nrows=1,
        ncols=1,
        figsize=(10, 8),
    )
    axes.axhline(0, color="k", linestyle=":")  # x = 0
    axes.axvline(0, color="k", linestyle=":")  # y = 0
    axes.axis("equal")

    plot_cell_names_on_uv = False

    daughter_cell_names = group_into_sublists(daughter_cell_names, 2)
    all_pointels = input_data.all_data[common_time_frame]["pointel coordinates"]
    pointel_label_lists = all_pointels.keys()
    for daughter_cells in daughter_cell_names:
        daughter_raw_labels = []
        for cell_name in daughter_cells:
            label = get_cell_label_at_time(
                cell_name, input_data.cell_name_to_label, common_time_frame
            )
            stripped_label = strip_labels([label])
            daughter_raw_labels.append(stripped_label[0])
            core_cells_pointel_keys = filter_pointel_labels(
                pointel_label_lists, stripped_label
            )
            core_cells_pointel_keys = filter_pointel_labels(
                core_cells_pointel_keys, CTJ.background
            )
            ctjs = []
            for pointel_labels in core_cells_pointel_keys:
                for cell in stripped_label:
                    if cell in pointel_labels:
                        ctjs.append(
                            CTJ(
                                all_pointels[pointel_labels],
                                cell,
                                pointel_labels,
                                common_time_frame,
                            )
                        )
            ctjs_cyclic = sort_ctj_list_in_cyclic_order(ctjs)
            coords_cyclic = [ctj.coordinates for ctj in ctjs_cyclic]
            ctj_vert_indices = []
            for coord in coords_cyclic:
                ctj_vert_dist = np.sqrt(
                    np.sum(np.square(mesh.vertices - coord), axis=1)
                )
                ctj_vert_index = np.arange(mesh.vertices.shape[0])[
                    ctj_vert_dist == np.min(ctj_vert_dist)
                ][0]
                ctj_vert_indices.append(ctj_vert_index)
            ctj_vert_indices.append(ctj_vert_indices[0])
            poly_u_coords = logmap.logmap[ctj_vert_indices, 0]
            poly_v_coords = logmap.logmap[ctj_vert_indices, 1]
            axes.plot(
                poly_u_coords,
                poly_v_coords,
                "bo-",
                alpha=0.1,
            )
            if plot_cell_names_on_uv:
                axes.text(
                    np.mean(poly_u_coords[:-1]),
                    np.mean(poly_v_coords[:-1]),
                    f"{cell_name}",
                    fontsize=9,
                    bbox=dict(facecolor='white', alpha=0.5),
                )
        pointels = filter_pointel_labels(pointel_label_lists, CTJ.background)
        for label in daughter_raw_labels:
            pointels = filter_pointel_labels(pointels, [label])
        if len(pointels) > 2:
            pointels = pointels[1:]
        division_line_vertices = [
            get_closest_coordinates_index(mesh.vertices, all_pointels[p][0])
            for p in pointels
        ]
        line_points = logmap.logmap[division_line_vertices, :]
        line_midpoint = np.mean(line_points, axis=0)
        vec_scale = 80
        perpendicular_vector = calculate_perpendicular_vector(
            line_points[0], line_points[1]
        )
        perpendicular_vector *= vec_scale
        perpendicular_vector_points_x = [
            line_midpoint[0] - perpendicular_vector[0],
            line_midpoint[0] + perpendicular_vector[0],
        ]
        perpendicular_vector_points_y = [
            line_midpoint[1] - perpendicular_vector[1],
            line_midpoint[1] + perpendicular_vector[1],
        ]
        axes.plot(
            perpendicular_vector_points_x,
            perpendicular_vector_points_y,
            "go-",
            ms=3,
            lw=5,
        )

    plt.show()
