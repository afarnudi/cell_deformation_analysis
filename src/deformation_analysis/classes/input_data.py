import copy
from deformation_analysis.tools.data_processing import load_pickle_file


class Input_data:
    def __init__(self, data_file_path, property_file_path) -> None:
        self.all_data = load_pickle_file(data_file_path)
        self.properties = load_pickle_file(property_file_path)
        self.cell_label_to_name = self.properties["cell_name"]
        self.cell_name_to_label = Input_data.get_cell_name_to_label_dict(
            self.cell_label_to_name
        )
        self.history = Input_data.get_cell_history(self.properties["cell_lineage"])
        self.embryo_path = "out/meshes/Astec-pm8/SURFACE_OBJ/Astec-Pm8_intrareg_post_t{0:03d}_1.obj"

    def get_cell_name_to_label_dict(cell_label_to_name):
        """Generate a name to label list dictionary.

        Use a label-name dictionary to build a reversed (name-label) dictionary.

        Parameters
        ----------
        cell_label_to_name : dict
            Dictionary with cell labels as keys and associated cell name as values.

        Returns
        -------
        dict
            Dictionary with cell names as keys and list of associated cell labels as values.
        """
        values = cell_label_to_name.values()
        cell_name_to_label = {v: [] for v in values}
        values = list(set(values))

        cell_label_to_name_copy = copy.deepcopy(cell_label_to_name)
        for name in values:
            used_keys = []
            for k, v in cell_label_to_name_copy.items():
                if v == name:
                    cell_name_to_label[name].append(k)
                    used_keys.append(k)
            for k in used_keys:
                del cell_label_to_name_copy[k]
        return cell_name_to_label

    def get_cell_history(cell_lineage):
        """Generate a dictionary containing all cell label history.

        Parameters
        ----------
        cell_lineage : dict
            Lineage dictionary with cell labels as keys and list of cell lineage as values.
            Typically the value of each cell label (key) is a list of length one, containing
            the label of the cell in the next time frame. In case of cell division, the list
            contains the two daughter cell labels.

        Returns
        -------
        dict
            Dictionary with cell labels as keys and list of all cell labels in time (sorted) as values.
        """
        history = {k: [k] for k in cell_lineage.keys()}
        for k, v in cell_lineage.items():
            if len(v) == 1:
                val = v[0]
                history[k].append(val)
                try:
                    history[val].append(k)
                except KeyError:
                    history[val] = [val]
                    history[val].append(k)
                for member in history[k]:
                    if member not in [k, val]:
                        history[member].append(val)
                        history[val].append(member)
        for k, v in history.items():
            history[k] = sorted(v)
        return history
