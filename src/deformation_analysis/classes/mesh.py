import potpourri3d as pp3d

class Mesh:
    def __init__(self, mesh_path) -> None:
        self.vertices, self.faces = pp3d.read_mesh(mesh_path)
        self.mesh_path = mesh_path
        self.scale = 3.3
        self.vertices *= self.scale