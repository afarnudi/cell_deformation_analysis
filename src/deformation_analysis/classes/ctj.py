import numpy as np

class CTJ:
    background = [1]

    def __init__(self, coordinates, cell_raw_label, pointel_labels, time_point):
        """
        Initilise a Cellular Tri-Junction object

        Parameters
        ----------
        coordinates : List
            A list of all coordinates measured for a pointel.
        cell_raw_label : int
            Label used in segmented image to represent the cell voxels.
        pointel_labels : tuple
            A tuple of integers (usually four) representing all image labels in contact with the pointel.
        time_point : int
            The time point of the image.
        """
        self.coordinates = np.mean(coordinates, axis=0)
        self.time_point = time_point
        self.cell_raw_label = cell_raw_label
        self.cell_label = self.get_cell_full_label(cell_raw_label)
        self.neighbour_labels, self.neighbour_raw_labels = self.get_neighbour_labels(
            pointel_labels
        )

    def get_cell_full_label(self, cell_raw_label):
        return self.time_point * 10000 + cell_raw_label

    def set_neighbour_lists(self, neighbour_labels, neighbour_raw_labels):
        self.neighbour_labels = neighbour_labels
        self.neighbour_raw_labels = neighbour_raw_labels

    def get_neighbour_labels(self, pointel_labels):
        neighbour_labels = []
        neighbour_raw_labels = []
        for raw_label in pointel_labels:
            if raw_label not in CTJ.background:
                if raw_label != self.cell_raw_label:
                    neighbour_raw_labels.append(raw_label)
                    neighbour_labels.append(self.get_cell_full_label(raw_label))
        return neighbour_labels, neighbour_raw_labels