import sys
import numpy as np
import matplotlib.pyplot as plt
from itertools import repeat

from deformation_analysis.tools.general import check_time_frames
from general.io import check_path_exists
from deformation_analysis.classes.input_data import Input_data
from deformation_analysis.classes.mesh import Mesh
from deformation_analysis.tools.data_processing import (
    generate_prefix_suffix_combination,
    get_cell_labels,
    get_cells_label_list,
    filter_pointel_labels,
    strip_labels,
    convert_labels_list_to_time_frames,
    check_frame_limits_consistency,
    sort_ctj_list_in_cyclic_order,
    convert_raw_labels_to_labels,
    get_coordinates_of_cell_labels,
    get_label_time,
    check_cell_names,
)
from deformation_analysis.tools.geometry import (
    reduce_coordinates_dimension,
    convert_cartesian_to_polar,
    convert_polar_to_cartesian,
    get_checkerboard_filter,
)
from deformation_analysis.tools.deformation import get_deformation_gradient_tensor
from deformation_analysis.tools.visualisation import visualise_ellipse_deformation
from deformation_analysis.classes.ctj import CTJ

import potpourri3d as pp3d


def find_cell_pointel_keys(all_pointels_at_time, cell_raw_labels):
    pointel_label_lists = all_pointels_at_time.keys()
    core_pointels = filter_pointel_labels(pointel_label_lists, cell_raw_labels)
    core_cells_pointel_keys = filter_pointel_labels(core_pointels, CTJ.background)
    return core_cells_pointel_keys


class Logmap:
    def __init__(
        self,
        time_point,
        solver,
        mesh,
        logmap,
        origin_vert_mesh_index,
        y_axis_vert_mesh_index,
        logmap_global_rotation,
    ) -> None:
        self.time_point = time_point
        self.solver = solver
        self.mesh = mesh

        logmap_polar = convert_cartesian_to_polar(logmap)
        logmap_polar[:, 1] += logmap_global_rotation
        rotated_logmap = convert_polar_to_cartesian(logmap_polar)
        self.logmap = rotated_logmap

        self.origin_vert_mesh_index = origin_vert_mesh_index
        self.y_axis_vert_mesh_index = y_axis_vert_mesh_index
        self.logmap_global_rotation = logmap_global_rotation

        x_vec = [1, 0]
        pt_local_rotation = self.solver.transport_tangent_vector(
            origin_vert_mesh_index,
            x_vec,
        )
        self.pt_local_rotation_angle = convert_cartesian_to_polar(pt_local_rotation)[
            :, 1
        ]





def get_closest_coordinates_index(vertices, coordinates):
    coord_vert_dist = np.sqrt(np.sum(np.square(vertices - coordinates), axis=1))

    coord_vert_index = np.arange(vertices.shape[0])[
        coord_vert_dist == np.min(coord_vert_dist)
    ][0]
    return coord_vert_index


def get_ctj(
    time_point,
    input_data,
    cell_name,
    cell_neighbour_names,
):
    all_pointels_at_time = input_data.all_data[time_point]["pointel coordinates"]

    cell_labels = get_cell_labels(
        input_data.cell_name_to_label,
        cell_name,
        time_point,
        input_data.history,
        input_data.properties,
    )
    cell_stripped_labels = strip_labels(cell_labels)
    pointel_label_lists = all_pointels_at_time.keys()
    core_cells_pointel_keys = filter_pointel_labels(
        pointel_label_lists, cell_stripped_labels
    )
    core_cells_pointel_keys = filter_pointel_labels(
        core_cells_pointel_keys, CTJ.background
    )
    for pointel_labels in core_cells_pointel_keys:
        for cell in cell_stripped_labels:
            if cell in pointel_labels:
                ctj = CTJ(
                    all_pointels_at_time[pointel_labels],
                    cell,
                    pointel_labels,
                    time_point,
                )
                if sorted(
                    [
                        input_data.cell_label_to_name[ctj.neighbour_labels[0]],
                        input_data.cell_label_to_name[ctj.neighbour_labels[1]],
                    ]
                ) == sorted(cell_neighbour_names):
                    return ctj


def generate_logmap(
    time_point,
    origin_ctj_cell_names,
    v_axis_ctj_cell_names,
    input_data,
    mesh,
):
    origin_ctj = get_ctj(
        time_point,
        input_data,
        origin_ctj_cell_names[0],
        origin_ctj_cell_names[1:],
    )

    v_axis_ctj = get_ctj(
        time_point,
        input_data,
        v_axis_ctj_cell_names[0],
        v_axis_ctj_cell_names[1:],
    )

    origin_vert_index = get_closest_coordinates_index(
        mesh.vertices, origin_ctj.coordinates
    )
    solver = pp3d.MeshVectorHeatSolver(mesh.vertices, mesh.faces)
    logmap = solver.compute_log_map(origin_vert_index)

    v_axis_vert_index = get_closest_coordinates_index(
        mesh.vertices, v_axis_ctj.coordinates
    )

    logmap_polar = convert_cartesian_to_polar(logmap)
    logmap_global_rotation = (
        np.pi / 2 - logmap_polar[v_axis_vert_index, 1]
    )  # align with the y axis

    logmap_first_time_point = Logmap(
        time_point,
        solver,
        mesh,
        logmap,
        origin_vert_index,
        v_axis_vert_index,
        logmap_global_rotation,
    )
    return logmap_first_time_point


def plot_logmap(logmap_t, ax, checkerboard):
    ax.scatter(
        logmap_t.mesh.vertices[:, 0],
        logmap_t.mesh.vertices[:, 1],
        logmap_t.mesh.vertices[:, 2],
        s=30,
        c=checkerboard,
        # alpha=0.3,
        depthshade=True,
    )
    ax.scatter(
        logmap_t.mesh.vertices[logmap_t.origin_vert_mesh_index][0] * 1.05,
        logmap_t.mesh.vertices[logmap_t.origin_vert_mesh_index][1] * 1.05,
        logmap_t.mesh.vertices[logmap_t.origin_vert_mesh_index][2] * 1.05,
        s=100,
        c="b",
        depthshade=True,
    )


def plot_parallel_transport_in_3D(
    logmap_t, source_point, vector, ax, ind_filter, fmt, vec_scale=30
):
    # Get inverse rotation matrix to rotate the logmap back to what is stored inside the solver
    theta = -logmap_t.logmap_global_rotation
    inverse_rotation_matrix = np.asarray(
        [[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]]
    )
    vector_in_solver_map = inverse_rotation_matrix @ np.asarray(vector)

    basisX, basisY, basisN = logmap_t.solver.get_tangent_frames()

    pt_vecs = logmap_t.solver.transport_tangent_vector(
        source_point,
        vector_in_solver_map,
    )  # parallel transported vecs

    pt_vecs_3D = pt_vecs[:, 0, np.newaxis] * basisX + pt_vecs[:, 1, np.newaxis] * basisY

    vec_scale = 30
    pt_vecs_beg = logmap_t.mesh.vertices * 1.04
    pt_vecs_end = pt_vecs_beg + pt_vecs_3D * vec_scale

    for i in ind_filter:
        ax.plot(
            [pt_vecs_beg[i, 0], pt_vecs_end[i, 0]],
            [pt_vecs_beg[i, 1], pt_vecs_end[i, 1]],
            [pt_vecs_beg[i, 2], pt_vecs_end[i, 2]],
            fmt,
        )
    ax.plot(
        [
            pt_vecs_beg[source_point, 0],
            pt_vecs_end[source_point, 0],
        ],
        [
            pt_vecs_beg[source_point, 1],
            pt_vecs_end[source_point, 1],
        ],
        [
            pt_vecs_beg[source_point, 2],
            pt_vecs_end[source_point, 2],
        ],
        "r-",
    )
    ax.plot(
        [pt_vecs_beg[source_point, 0]],
        [pt_vecs_beg[source_point, 1]],
        [pt_vecs_beg[source_point, 2]],
        "ro-",
    )


def plot_2D_vectors(ext, ax, vertices, ind_filter, fmt, vec_scale=30, lw=2):
    begs = vertices[ind_filter]
    ends = vertices[ind_filter] + ext[ind_filter] * vec_scale
    for i in range(len(begs)):
        ax.plot(
            [begs[i, 0], ends[i, 0]],
            [begs[i, 1], ends[i, 1]],
            fmt,
            lw=lw,
        )

def calculate_perpendicular_vector(point1, point2):
    """
    Calculate the unit perpendicular vector to the line segment
    defined by two points.

    Parameters
    ----------
    point1 : tuple
        The coordinates of the first point (x, y).
    point2 : tuple
        The coordinates of the second point (x, y).

    Returns
    -------
    numpy.ndarray
        The unit perpendicular vector to the line segment.
    """
    dx = point2[0] - point1[0]
    dy = point2[1] - point1[1]
    
    perpendicular_vector = np.asarray([-dy, dx])
    return perpendicular_vector / np.linalg.norm(perpendicular_vector)


def cell_single_shape_analysis_manager(user_args):
    cell_names = user_args.cell_names
    cell_names = generate_prefix_suffix_combination(cell_names, ["_", "*"])

    data_file_path = user_args.processed_file_path
    property_file_path = user_args.property_file_path
    check_path_exists(data_file_path)
    check_path_exists(property_file_path)
    check_time_frames(user_args.frame_limit)

    input_data = Input_data(data_file_path, property_file_path)

    check_cell_names(cell_names, input_data.cell_name_to_label)

    # Check if user time frame limit input is consistent with the cells' lifetime
    cells_label_list = get_cells_label_list(cell_names, input_data.cell_name_to_label)
    cell_frames = convert_labels_list_to_time_frames(cells_label_list)
    frame_limits = check_frame_limits_consistency(user_args.frame_limit, cell_frames)

    analysis_time_steps = np.arange(frame_limits[0], frame_limits[1] + 1)
    max_time_interval = frame_limits[1] - frame_limits[0]
    analysis_end_time = analysis_time_steps[-1]

    plot_mesh_logmap = False
    plot_checkerboard_on_uv = True
    plot_cell_names_on_uv = False
    plot_parallel_transport_vecs = False
    plot_cell_division_direction = True

    xlabelpad = 8
    ylabelpad = 2
    legendfont = 18
    fontsize = 16
    plt.rcParams["font.family"] = "Times New Roman"
    plt.rcParams["mathtext.fontset"] = "stix"
    plt.rcParams.update({"font.size": fontsize})
    visualisation = True
    save_fig = True
    fig, axes = plt.subplots(
        nrows=1,
        ncols=3,
        figsize=(16, 8),
        subplot_kw=dict(projection="3d", aspect="equal"),
    )
    axes[0].shareview(axes[1])
    axes[1].shareview(axes[2])

    ## logmap construction

    check_cell_names(user_args.mesh_map_source_axis, input_data.cell_name_to_label)
    origin_ctj_cell_names = user_args.mesh_map_source_axis[:3]
    v_axis_ctj_cell_names = user_args.mesh_map_source_axis[3:]

    logmap_time_point = user_args.frame_limit[0]
    mesh = Mesh(input_data.embryo_path.format(logmap_time_point))
    # logmap, solver, checkerboard = generate_logmap(
    logmap_first_time_point = generate_logmap(
        logmap_time_point,
        origin_ctj_cell_names,
        v_axis_ctj_cell_names,
        input_data,
        mesh,
    )

    checkerboard = get_checkerboard_filter(logmap_first_time_point.logmap, 60)
    # checkerboard = get_checkerboard_filter(logmap_polar, 20000 / np.pi, "polar norm")
    checkerboard = get_checkerboard_filter(
        convert_cartesian_to_polar(logmap_first_time_point.logmap),
        60,
        "polar",
        np.pi / 4,
    )
    # checkerboard = get_checkerboard_filter(logmap_polar, 60, "polar", 0)
    if plot_mesh_logmap:
        plot_logmap(logmap_first_time_point, axes[2], checkerboard)

    fig_uv, axes_uv = plt.subplots(
        nrows=1,
        ncols=1,
        figsize=(10, 8),
        # subplot_kw={'projection': 'polar'},
        # subplot_kw=dict(projection="3d", aspect="equal"),
    )

    ctjs_in_time_dict = {}

    for cell_name in cell_names:
        try:
            # Get cell's reference CTJs (cellular junction points).
            time_point = analysis_time_steps[0]
            # time_point = 18
            all_pointels_at_time = input_data.all_data[time_point][
                "pointel coordinates"
            ]

            cell_labels = get_cell_labels(
                input_data.cell_name_to_label,
                cell_name,
                time_point,
                input_data.history,
                input_data.properties,
            )
            cell_stripped_labels = strip_labels(cell_labels)
            pointel_label_lists = all_pointels_at_time.keys()
            core_cells_pointel_keys = filter_pointel_labels(
                pointel_label_lists, cell_stripped_labels
            )
            core_cells_pointel_keys = filter_pointel_labels(
                core_cells_pointel_keys, CTJ.background
            )
            # core_cells_pointel_keys = find_cell_pointel_keys(
            #     all_pointels_at_time, cell_stripped_labels
            # )

            ctjs = []
            for pointel_labels in core_cells_pointel_keys:
                for cell in cell_stripped_labels:
                    if cell in pointel_labels:
                        ctjs.append(
                            CTJ(
                                all_pointels_at_time[pointel_labels],
                                cell,
                                pointel_labels,
                                time_point,
                            )
                        )
            ctjs_cyclic = sort_ctj_list_in_cyclic_order(ctjs)

            ctjs_in_time = []
            all_cell_neighbours = []
            for ctj in ctjs_cyclic:
                all_cell_neighbours.append(
                    input_data.cell_label_to_name[ctj.neighbour_labels[0]]
                )
                all_cell_neighbours.append(
                    input_data.cell_label_to_name[ctj.neighbour_labels[1]]
                )
            all_cell_neighbours = list(set(all_cell_neighbours))
            print(f"all_cell_neighbours = {all_cell_neighbours}")
            for i_cyc, ctj in enumerate(ctjs_cyclic):
                ctj_time_evolution = [ctj]

                print(
                    f"core cell  [{input_data.cell_label_to_name[ctj.cell_label]}]\n"
                    f"neighbours [{input_data.cell_label_to_name[ctj.neighbour_labels[0]]} "
                    f"{input_data.cell_label_to_name[ctj.neighbour_labels[1]]}]\n"
                )

                for time_point in analysis_time_steps[1:]:
                    last_ctj = ctj_time_evolution[-1]

                    cell_raw_labels, new_neighbours_1, new_neighbours_2 = list(
                        map(
                            get_cell_labels,
                            repeat(input_data.cell_name_to_label),
                            [
                                input_data.cell_label_to_name[last_ctj.cell_label],
                                input_data.cell_label_to_name[
                                    last_ctj.neighbour_labels[0]
                                ],
                                input_data.cell_label_to_name[
                                    last_ctj.neighbour_labels[1]
                                ],
                            ],
                            repeat(time_point),
                            repeat(input_data.history),
                            repeat(input_data.properties),
                        )
                    )  # get lists of cell names / daughters / grand children/ ... at the selected time point
                    cell_raw_labels = strip_labels(cell_raw_labels)
                    new_neighbours_1 = strip_labels(new_neighbours_1)
                    new_neighbours_2 = strip_labels(new_neighbours_2)

                    all_pointels_at_time = input_data.all_data[time_point][
                        "pointel coordinates"
                    ]
                    pointel_label_lists = all_pointels_at_time.keys()
                    core_pointels = filter_pointel_labels(
                        pointel_label_lists, cell_raw_labels
                    )
                    core_cells_pointel_keys = filter_pointel_labels(
                        core_pointels, CTJ.background
                    )
                    # core_cells_pointel_keys = find_cell_pointel_keys(
                    #     all_pointels_at_time, cell_raw_labels
                    # )

                    ctjs_with_both_neighbour_sets_and_background = []
                    for pointel_labels in core_cells_pointel_keys:
                        for cell in cell_raw_labels:
                            if cell in pointel_labels:
                                if set(new_neighbours_1) & set(pointel_labels) and set(
                                    new_neighbours_2
                                ) & set(pointel_labels):
                                    ctjs_with_both_neighbour_sets_and_background.append(
                                        CTJ(
                                            all_pointels_at_time[pointel_labels],
                                            cell,
                                            pointel_labels,
                                            time_point,
                                        )
                                    )
                    if len(ctjs_with_both_neighbour_sets_and_background) == 1:
                        ctj_time_evolution.append(
                            ctjs_with_both_neighbour_sets_and_background[0]
                        )
                    elif len(ctjs_with_both_neighbour_sets_and_background) == 0:
                        print()
                        print(f"Difficulty tracking point at time step {time_point}\n")

                        labels_with_just_the_neighbour_sets = []
                        for pointel_labels in core_pointels:
                            for cell in cell_raw_labels:
                                if cell in pointel_labels:
                                    if set(new_neighbours_1) & set(
                                        pointel_labels
                                    ) and set(new_neighbours_2) & set(pointel_labels):
                                        labels_with_just_the_neighbour_sets.append(
                                            pointel_labels
                                        )
                        if len(labels_with_just_the_neighbour_sets) == 1:
                            new_candidate = labels_with_just_the_neighbour_sets[0]
                            print(f"Likely candidate: {new_candidate} between:")
                            for label in new_candidate:
                                full_label = time_point * 10000 + label
                                print(
                                    input_data.cell_label_to_name[full_label], end="  "
                                )
                            print()
                            for cell in cell_raw_labels:
                                if cell in new_candidate:
                                    neighbours = []
                                    print(new_neighbours_1, new_neighbours_2)
                                    for neighbour_list in [
                                        new_neighbours_1,
                                        new_neighbours_2,
                                    ]:
                                        for neighbour in neighbour_list:
                                            neighbours.extend(
                                                set(new_candidate) & set([neighbour])
                                            )

                                    if len(neighbours) == 2:
                                        ctj = CTJ(
                                            all_pointels_at_time[new_candidate],
                                            cell,
                                            new_candidate,
                                            time_point,
                                        )
                                        # Since we do not have the background among the labels, we have to manually assign the correct neighbours.
                                        neighbours_labels = (
                                            convert_raw_labels_to_labels(
                                                neighbours, time_point
                                            )
                                        )
                                        ctj.set_neighbour_lists(
                                            neighbours_labels, neighbours
                                        )
                                        ctj_time_evolution.append(ctj)
                                        temp_labels = []
                                        for label in neighbours:
                                            full_label = time_point * 10000 + label
                                            temp_labels.append(
                                                input_data.cell_label_to_name[
                                                    full_label
                                                ]
                                            )
                                        print(
                                            f"switching to: \n{cell_name}\n{temp_labels[0]}\n{temp_labels[1]}"
                                        )

                                    else:
                                        print(f"Wrong neighbour list length:")

                                        break
                        else:
                            labels_with_either_of_the_neighbour_sets = []
                            for pointel_labels in core_cells_pointel_keys:
                                for cell in cell_raw_labels:
                                    if cell in pointel_labels:
                                        if set(new_neighbours_1) & set(
                                            pointel_labels
                                        ) or set(new_neighbours_2) & set(
                                            pointel_labels
                                        ):
                                            labels_with_either_of_the_neighbour_sets.append(
                                                pointel_labels
                                            )
                            print(core_cells_pointel_keys)
                            print(labels_with_either_of_the_neighbour_sets)
                            all_potential_cell_neighbours = []
                            neighbours_cell_background = set.union(
                                set(new_neighbours_1),
                                set(new_neighbours_2),
                                set(cell_raw_labels),
                                set(CTJ.background),
                            )
                            print(neighbours_cell_background)
                            for labels in labels_with_either_of_the_neighbour_sets:
                                for label in labels:
                                    if label not in neighbours_cell_background:
                                        all_potential_cell_neighbours.append(label)
                            print(all_potential_cell_neighbours)
                            seen_labels = set()
                            duplicate_labels = []
                            for label in all_potential_cell_neighbours:
                                if label in seen_labels:
                                    duplicate_labels.append(label)
                                else:
                                    seen_labels.add(label)
                            if len(duplicate_labels) == 1:
                                neighbour_switched_cell = duplicate_labels[0]
                                ctjs_of_T_switch = []
                                for neighbours in [new_neighbours_1, new_neighbours_2]:
                                    for pointel_labels in core_cells_pointel_keys:
                                        for cell in cell_raw_labels:
                                            if cell in pointel_labels:
                                                if set([neighbour_switched_cell]) & set(
                                                    pointel_labels
                                                ) and set(neighbours) & set(
                                                    pointel_labels
                                                ):
                                                    ctjs_of_T_switch.append(
                                                        CTJ(
                                                            all_pointels_at_time[
                                                                pointel_labels
                                                            ],
                                                            cell,
                                                            pointel_labels,
                                                            time_point,
                                                        )
                                                    )
                                ctj_time_evolution.append(
                                    ctjs_of_T_switch[0]
                                )  # It does not matter which one we pick since the last ctj split in two
                            elif len(duplicate_labels) == 0:
                                # print('Here')
                                # ctjs_of_T_switch_merge=[]
                                for cell in all_potential_cell_neighbours:
                                    print(
                                        cell,
                                        input_data.cell_label_to_name[
                                            time_point * 10000 + cell
                                        ],
                                    )
                                    candidate_cell_labels_with_original_cell_or_neighbours = (
                                        []
                                    )
                                    potential_neighbour_core_pointels = (
                                        filter_pointel_labels(
                                            pointel_label_lists, [cell]
                                        )
                                    )
                                    potential_neighbour_core_pointels = (
                                        filter_pointel_labels(
                                            potential_neighbour_core_pointels,
                                            CTJ.background,
                                        )
                                    )
                                    # print(potential_neighbour_core_pointels)
                                    for (
                                        pointel_labels
                                    ) in potential_neighbour_core_pointels:
                                        # for cell in cell_raw_labels:
                                        #     if cell in pointel_labels:
                                        if set(cell_raw_labels) & set(
                                            pointel_labels
                                        ) and set(new_neighbours_1) & set(
                                            pointel_labels
                                        ):
                                            candidate_cell_labels_with_original_cell_or_neighbours.append(
                                                pointel_labels
                                            )
                                        if set(cell_raw_labels) & set(
                                            pointel_labels
                                        ) and set(new_neighbours_2) & set(
                                            pointel_labels
                                        ):
                                            candidate_cell_labels_with_original_cell_or_neighbours.append(
                                                pointel_labels
                                            )
                                        if set(new_neighbours_2) & set(
                                            pointel_labels
                                        ) and set(new_neighbours_1) & set(
                                            pointel_labels
                                        ):
                                            candidate_cell_labels_with_original_cell_or_neighbours.append(
                                                pointel_labels
                                            )
                                    # print(candidate_cell_labels_with_original_cell_and_neighbours)
                                    if (
                                        len(
                                            candidate_cell_labels_with_original_cell_or_neighbours
                                        )
                                        == 2
                                    ):
                                        break
                                if (
                                    len(
                                        candidate_cell_labels_with_original_cell_or_neighbours
                                    )
                                    == 2
                                ):
                                    neighbour_switched_cell = cell
                                    for (
                                        labels
                                    ) in candidate_cell_labels_with_original_cell_or_neighbours:
                                        for original_cell in cell_raw_labels:
                                            if original_cell in labels:
                                                merged_ctj = CTJ(
                                                    all_pointels_at_time[labels],
                                                    original_cell,
                                                    labels,
                                                    time_point,
                                                )

                                    ctj_time_evolution.append(
                                        merged_ctj
                                    )  # It does not matter which one we pick since the last ctj split in two

                            else:
                                for title, raw_labels in zip(
                                    [
                                        "core cells",
                                        "neighbour",
                                        "neighbour",
                                    ],
                                    [
                                        cell_raw_labels,
                                        new_neighbours_1,
                                        new_neighbours_2,
                                    ],
                                ):
                                    print(f"{title}: {raw_labels}  ", end="")
                                    for label in convert_raw_labels_to_labels(
                                        raw_labels, time_point
                                    ):
                                        print(
                                            f"{input_data.cell_label_to_name[label]}  "
                                        )
                                    print()
                                print(
                                    f"list of core_cells_pointel_keys:\n{core_cells_pointel_keys}"
                                )
                                print(f"list of core_pointels:\n{core_pointels}")
                                break
                    else:
                        print(f"something went wrong at time step {time_point}\n")
                        for title, raw_labels in zip(
                            [
                                "core cells",
                                "neighbour",
                                "neighbour",
                            ],
                            [
                                cell_raw_labels,
                                new_neighbours_1,
                                new_neighbours_2,
                            ],
                        ):
                            print(f"{title}: {raw_labels}  ", end="")
                            for label in convert_raw_labels_to_labels(
                                raw_labels, time_point
                            ):
                                print(f"{input_data.cell_label_to_name[label]}  ")
                            print()
                        print(
                            f"list of core_cells_pointel_keys:\n{core_cells_pointel_keys}"
                        )
                        break

                print(f"================ t= {time_point}")
                ctjs_in_time.append(ctj_time_evolution)
            print("---------------")

            ctjs_in_time = [
                ctj_list[:max_time_interval]
                for ctj_list in ctjs_in_time
                if len(ctj_list) >= max_time_interval
            ]
            if len(ctjs_in_time) < 3:
                Exception(
                    f"Could not track enough CTJs for deformation calculations. I managed to track {len(ctjs_in_time)}"
                )
        except Exception as e:
            print(f"Could not process {cell_name}\n{str(e)}")
            raise
        cell_labels = get_cell_labels(
            input_data.cell_name_to_label,
            cell_name,
            analysis_end_time,
            input_data.history,
            input_data.properties,
        )
        cell_stripped_labels = strip_labels(cell_labels)
        (
            all_points_at_end,
            edge_coords_at_end,
            non_edge_coords_at_end,
        ) = get_coordinates_of_cell_labels(
            cell_stripped_labels,
            input_data.all_data[analysis_end_time]["pointel coordinates"],
        )

        beg_ctjs = [ctj_list[0] for ctj_list in ctjs_in_time]
        beg_coords = np.asarray([ctj.coordinates for ctj in beg_ctjs])

        end_ctjs = [ctj_list[-1] for ctj_list in ctjs_in_time]
        end_coords = np.asarray([ctj.coordinates for ctj in end_ctjs])

        # Project points on the best-fit plane and rotate plane onto the XY plane
        end_coords, _ = reduce_coordinates_dimension(end_coords)
        beg_coords, _ = reduce_coordinates_dimension(beg_coords)

        deformation_gradient_tensor = get_deformation_gradient_tensor(
            beg_coords, end_coords
        )
        right_Cauchy_Green_tensor = (
            deformation_gradient_tensor.T @ deformation_gradient_tensor
        )
        eigenvalues, eigenvectors = np.linalg.eig(np.matrix(right_Cauchy_Green_tensor))
        # eigenvalues of the right stretch tensor are the sqrt of the right Cauchy-Green Tensor's eigenvalues: U^2 = F^T@F
        eigenvalues = np.sqrt(eigenvalues)

        # Rotate the ellipse on the XY plane, back to its original orientation before alining the first point with the x-axis
        # eigenvectors = beg_rot.T @ eigenvectors
        tracked_coordinates_in_time = []
        for i in range(len(ctjs_in_time[0])):
            ctjs = [ctj_list[i] for ctj_list in ctjs_in_time]
            tracked_coordinates_in_time.append([ctj.coordinates for ctj in ctjs])
        tracked_coordinates_in_time = np.asarray(tracked_coordinates_in_time)

        # visulise cells in UV coordinates

        axes_uv.axhline(0, color="k", linestyle=":")  # x = 0
        axes_uv.axvline(0, color="k", linestyle=":")  # y = 0

        ctjs_in_time_dict[cell_name] = ctjs_in_time

        ctj_vert_indices = []
        for coord in tracked_coordinates_in_time[0]:
            ctj_vert_dist = np.sqrt(np.sum(np.square(mesh.vertices - coord), axis=1))
            ctj_vert_index = np.arange(mesh.vertices.shape[0])[
                ctj_vert_dist == np.min(ctj_vert_dist)
            ][0]
            ctj_vert_indices.append(ctj_vert_index)
        ctj_vert_indices.append(ctj_vert_indices[0])

        poly_u_coords = logmap_first_time_point.logmap[ctj_vert_indices, 0]
        poly_v_coords = logmap_first_time_point.logmap[ctj_vert_indices, 1]

        axes_uv.plot(
            poly_u_coords,
            poly_v_coords,
            "bo-",
        )

        if cell_name == "a7.0011_":
            origin_3D_tracked_coords = tracked_coordinates_in_time[:, 5]

        if cell_name == "b7.0016_":
            yaxis_3D_tracked_coords = tracked_coordinates_in_time[:, 1]

        if plot_cell_names_on_uv:
            axes_uv.text(
                np.mean(poly_u_coords[:-1]),
                np.mean(poly_v_coords[:-1]),
                f"{cell_name}",
            )
        axes_uv.axis("equal")

        visualise_ellipse_deformation(
            tracked_coordinates_in_time,
            all_points_at_end,
            eigenvalues,
            eigenvectors,
            axes=axes,
        )

    if plot_cell_division_direction:
        for cell_name in cell_names:
            time_point = analysis_time_steps[0]
            cell_labels = get_cell_labels(
                input_data.cell_name_to_label,
                cell_name,
                time_point,
                input_data.history,
                input_data.properties,
            )[0]
            label_before_division = input_data.history[cell_labels][-1]
            daughter_cell_labels = input_data.properties["cell_lineage"][
                label_before_division
            ]
            division_time_point = get_label_time(daughter_cell_labels[0])
            all_pointels_at_time = input_data.all_data[division_time_point][
                "pointel coordinates"
            ]
            daughter_cell_raw_labels = strip_labels(daughter_cell_labels)
            pointel_label_lists = all_pointels_at_time.keys()
            pointels = filter_pointel_labels(pointel_label_lists, CTJ.background)
            for label in daughter_cell_raw_labels:
                pointels = filter_pointel_labels(pointels, [label])
            if len(pointels) == 2:
                print(division_time_point)
                division_time_ind = np.where(
                    analysis_time_steps == division_time_point
                )[0][0]
                mesh = Mesh(input_data.embryo_path.format(division_time_point))
                # print(division_time_ind)
                # print(origin_3D_tracked_coords[division_time_ind])
                # print(yaxis_3D_tracked_coords[division_time_ind])
                origin_vert_index = get_closest_coordinates_index(
                    mesh.vertices, origin_3D_tracked_coords[division_time_ind]
                )
                solver = pp3d.MeshVectorHeatSolver(mesh.vertices, mesh.faces)
                logmap_division = solver.compute_log_map(origin_vert_index)

                v_axis_vert_index = get_closest_coordinates_index(
                    mesh.vertices, yaxis_3D_tracked_coords[division_time_ind]
                )

                logmap_division_polar = convert_cartesian_to_polar(logmap_division)
                logmap_global_rotation = (
                    np.pi / 2 - logmap_division_polar[v_axis_vert_index, 1]
                )  # align with the y axis

                logmap_division = Logmap(
                    time_point,
                    solver,
                    mesh,
                    logmap_division,
                    origin_vert_index,
                    v_axis_vert_index,
                    logmap_global_rotation,
                )
                division_line_vertices = [
                    get_closest_coordinates_index(
                        mesh.vertices, all_pointels_at_time[p][0]
                    )
                    for p in pointels
                ]
                print(division_line_vertices)
                print("**")
                
                ctjs_in_time = ctjs_in_time_dict[cell_name]
                
                tracked_coordinates_in_time = []
                for i in range(len(ctjs_in_time[0])):
                    ctjs = [ctj_list[i] for ctj_list in ctjs_in_time]
                    tracked_coordinates_in_time.append([ctj.coordinates for ctj in ctjs])
                tracked_coordinates_in_time = np.asarray(tracked_coordinates_in_time)

                ctj_vert_indices = []
                for coord in tracked_coordinates_in_time[division_time_ind]:
                    ctj_vert_dist = np.sqrt(np.sum(np.square(mesh.vertices - coord), axis=1))
                    ctj_vert_index = np.arange(mesh.vertices.shape[0])[
                        ctj_vert_dist == np.min(ctj_vert_dist)
                    ][0]
                    ctj_vert_indices.append(ctj_vert_index)
                ctj_vert_indices.append(ctj_vert_indices[0])

                poly_u_coords = logmap_division.logmap[ctj_vert_indices, 0]
                poly_v_coords = logmap_division.logmap[ctj_vert_indices, 1]

                axes_uv.plot(
                    poly_u_coords,
                    poly_v_coords,
                    "mo-",
                )


                line_points = logmap_division.logmap[division_line_vertices, :]
                line_midpoint = np.mean(line_points,axis=0)
                axes_uv.plot(
                    line_points[:, 0],
                    line_points[:, 1],
                    "go:",
                    ms=2,
                )                
                perpendicular_vector = calculate_perpendicular_vector(line_points[0],line_points[1])
                perpendicular_vector_points_x = [line_midpoint[0]-perpendicular_vector[0], line_midpoint[0]+perpendicular_vector[0]]
                perpendicular_vector_points_y = [line_midpoint[1]-perpendicular_vector[1], line_midpoint[1]+perpendicular_vector[1]]
                axes_uv.plot(
                    perpendicular_vector_points_x,
                    perpendicular_vector_points_y,
                    "go-",
                    ms=3,
                )


            else:
                Exception(
                    f"Found {len(pointels)} common ctjs between cell daughters at division time."
                )

    if plot_checkerboard_on_uv:
        axes_uv.scatter(
            logmap_first_time_point.logmap[:, 0],
            logmap_first_time_point.logmap[:, 1],
            s=30,
            c=checkerboard,
            alpha=0.05,
            edgecolors="none",
            # depthshade=True,
        )
    if plot_parallel_transport_vecs:
        logmap_polar = convert_cartesian_to_polar(logmap_first_time_point.logmap)
        ind_filter = np.arange(logmap_first_time_point.logmap.shape[0])[
            logmap_polar[:, 0] < 60
        ][::4]
        ind_filter = np.append(
            ind_filter, logmap_first_time_point.origin_vert_mesh_index
        )
        ind_filter = np.append(
            ind_filter, logmap_first_time_point.y_axis_vert_mesh_index
        )

        for vec, point, fmt in zip(
            [[0.0, 1.0], [-0.5, -0.5]],
            [
                logmap_first_time_point.origin_vert_mesh_index,
                logmap_first_time_point.y_axis_vert_mesh_index,
            ],
            ["k-", "b--"],
        ):

            # adjust selected vector for solver's local random rotation
            vec_polar = convert_cartesian_to_polar(vec)
            vec_polar[1] += logmap_first_time_point.pt_local_rotation_angle[point]
            vec = convert_polar_to_cartesian(vec_polar)

            plot_parallel_transport_in_3D(
                logmap_first_time_point,
                point,
                vec,
                axes[2],
                ind_filter,
                fmt,
            )

            pt_vecs = logmap_first_time_point.solver.transport_tangent_vector(
                point,
                vec,
            )
            # rotate back all parallel transported vectors (make correction for the solver's local random rotation)
            pt_vecs_polar = convert_cartesian_to_polar(pt_vecs)
            pt_vecs_polar[:, 1] -= logmap_first_time_point.pt_local_rotation_angle
            pt_vecs = convert_polar_to_cartesian(pt_vecs_polar)

            plot_2D_vectors(
                pt_vecs,
                axes_uv,
                logmap_first_time_point.logmap,
                ind_filter,
                fmt,
            )
            plot_2D_vectors(
                pt_vecs,
                axes_uv,
                logmap_first_time_point.logmap,
                [point],
                "r",
                lw=4,
            )

    fig.tight_layout()
    plt.show()
