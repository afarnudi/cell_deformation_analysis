from general.argument_parsing import get_user_arguments
from Astec_image_processing_tools.process_Astec_images import (
    process_Astec_images_manager,
)
from Astec_image_processing_tools.merge_pickle_files import merge_pickle_files_manager
from deformation_analysis.cell_shape_analysis import cell_single_shape_analysis_manager
from deformation_analysis.cell_division_analysis import cell_division_analysis


def run():
    user_args = get_user_arguments()
    if user_args.command == "process-images":
        process_Astec_images_manager(user_args)
    elif user_args.command == "merge":
        merge_pickle_files_manager(user_args)
    elif user_args.command == "cell-shape-analyser":
        if user_args.cell_division_analysis:
            cell_division_analysis(user_args)
        else:
            cell_single_shape_analysis_manager(user_args)


if __name__ == "__main__":
    run()
